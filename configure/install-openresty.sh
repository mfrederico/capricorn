#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

export RESTY_VER=1.21.4.2

if [ -z "$WEBUSER" ]; then
	echo "Please run sudo WEBUSER=yourwebuser $0"
	exit 1
fi

CWD=$(pwd)

# Voodoo to grab tools folder
SCRIPT_LOC=$(/usr/bin/dirname $0)
cd $SCRIPT_LOC; cd ..
CONF_LOC=$(pwd)
cd $CWD

echo "* Installing requirements"
sudo ntpdate -v time.google.com

apt-get -y install luarocks 
apt-get -y install 'libpcre++-dev'
apt-get -y install libgeoip-dev openssl libssl-dev composer nodejs npm dnsmasq

echo "* Making sure we have zlib installed"
apt-get -y install zlib1g-dev

# apt-get -y heirloom-mailx

echo "* Installing openresty lua libraries"
./install-luarocks.sh

echo -n "* Getting ready to configure: "

apt-get -y install --no-install-recommends wget gnupg ca-certificates
wget -O - https://openresty.org/package/pubkey.gpg | sudo gpg --dearmor -o /usr/share/keyrings/openresty.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/openresty.gpg] http://openresty.org/package/ubuntu $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/openresty.list > /dev/null

echo "* Installing"
sudo apt-get update
sudo apt-get -y install --no-install-recommends openresty

echo "* Enabling webservice"
mkdir /etc/nginx/sites-enabled
mkdir -p /var/www/storage
mkdir -p /var/www/letsencrypt
mkdir -p /var/www/html/default
mv /usr/local/openresty/nginx/conf /usr/local/openresty/nginx/conf-orig
mv /var/log/nginx /var/log/nginx-old
ln -s /etc/openresty/ /etc/nginx
ln -s /etc/nginx/sites-available/ /etc/nginx/sites-enabled/
ln -s $CONF_LOC/etc/nginx /usr/local/openresty/nginx/conf
ln -s /usr/local/openresty/nginx/logs/ /var/log/nginx/
ln -s /usr/bin/openresty /usr/bin/nginx

cd $CWD

echo "* Creating cachebody directory"
mkdir -p /var/lib/nginx/body

service openresty start

echo "* ./install-letsencrypt.sh next.. "
