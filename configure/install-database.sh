#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

/usr/bin/wget https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
source ./mariadb_repo_setup

VERSION=10.11.5
LSB=$(lsb_release -sc)

# Voodoo to grab my settings
SCRIPT_LOC=$(/usr/bin/dirname $0)
cd $SCRIPT_LOC

if [ ! -f ../settings.conf ]; then
        echo "* Please see settings.example.conf and create a settings.conf file."
        exit 1
else
        echo "* Loading config: $(pwd)/../settings.conf"
        source ../settings.conf || exit_on_error "Cannot source settings."
fi

MYIP=$(ip -4 address | grep -m 1 "global" | cut -d " " -f6 | cut -d "/" -f1)
ISMASTER=0

apt purge -y mysql*
apt -y autoremove
apt autoclean

echo "****************************************"
echo "* This Node IP address:  $MYIP"
read -t 30 -p "Set root password for this server : [$MYSQL_PASS] " -e INPUT
MYSQL_PASS=${INPUT:-$MYSQL_PASS}

read -t 30 -p "Master node ip to replicate       : [$MYSQL_MASTER] " -e INPUT2
MYSQL_MASTER=${INPUT2:-$MYSQL_MASTER}
if [[ -z "$MYSQL_MASTER" ]]; then
	MYSQL_MASTER="127.0.0.1"
fi

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_PASS"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_PASS"

apt-get install -y software-properties-common
sudo apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'

apt update
apt install -y mariadb-server

if [[ "$MYSQL_MASTER" == "$MYIP" ]]; then
	echo "* This server will be used as the master node."
	ISMASTER=1
	CLUSTERADDY="gcomm://$MYSQL_MASTER"
else
	echo "* This is a new node $MYIP which will be replicating $MYSQL_MASTER"
	CLUSTERADDY="gcomm://$MYSQL_MASTER,$MYIP"
fi

echo "* wsrep default is off"
echo "* Master node: $MYSQL_MASTER"

echo "[galera]
wsrep_on=OFF
wsrep_provider=/usr/lib/galera/libgalera_smm.so
wsrep_cluster_address=$CLUSTERADDY
binlog_format=row
default_storage_engine=InnoDB
innodb_flush_log_at_trx_commit=0
innodb_thread_concurrency=0
transaction-isolation=READ-COMMITTED
wsrep_provider_options=\"gcache.size=300M; gcache.page_size=300M\"
innodb_autoinc_lock_mode=2" > /etc/mysql/mariadb.conf.d/60-galera.cnf
echo "* Galera config saved at /etc/mysql/mariadb.conf.d/60-galera.cnf"

echo "* Updating bind address to listen on all interfaces"
sed -i -e 's/bind-address.*/bind-address=0.0.0.0/' /etc/mysql/maraidb.conf.d/50-server.cnf
sed -i -e 's/max_connections.*$/max_connections=500/' /etc/mysql/maraidb.conf.d/50-server.cnf

service mysql stop
kill -9 $(pgrep mysql)

echo "************************************"
echo "IF things act weird, do this: "
echo "/usr/bin/mysqld_safe --skip-grant-tables & 
echo \"use mysql; 
update user set password=PASSWORD('$MYSQL_PASS') where User='root';
update user set plugin='mysql_native_password' where User='root'; 
GRANT SELECT, INSERT, UPDATE, DELETE, DELETE HISTORY, CREATE, DROP, REFERENCES, INDEX, ALTER, SHOW DATABASES, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EVENT, TRIGGER ON *.* TO 'root'@'$MYIP' WITH GRANT OPTION; flush privileges;
flush privileges;\" | mysql -u root"
echo "Install TimeZone: mysql_tzinfo_to_sql /usr/share/zoneinfo/ | sudo mysql -u root mysql"
echo "************************************"

if [[ "$ISMASTER" -eq 1 ]]; then
	echo "******************************************"
	echo "* If using replication allow these ports *"
	echo "******************************************"
	echo "ufw allow 3306" 
	echo "ufw allow 4567"
	echo "ufw allow 4567/udp"
	echo "ufw allow 4568"
	echo "ufw allow 4444"

	echo "NEW CLUSTER STARTING"
	/usr/bin/galera_new_cluster
else
	echo "NODE STARTING $MYSQL_MASTER"
	service mariadb start 
fi
