#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

if [ ! -e /usr/bin/luarocks ]; then
	apt-get -y install luarocks
	apt-get -y install openssl 
	apt-get -y install libssl1.0-dev
fi

ROCKS="lua-resty-jit-uuid lua-resty-libcjson lua-resty-cookie luafilesystem cjson bcrypt lua-resty-mysql lua-resty-jwt"

for ROCKFEST in $ROCKS; 
do
	ROCKEXISTS=$(/usr/bin/luarocks list | grep $ROCKFEST)

    if [ -z "$ROCKEXISTS" ]; then
		/usr/bin/luarocks install $ROCKFEST
    else
        echo "* SKIP $ROCKFEST"
    fi
done
