#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Voodoo to grab my settings
SCRIPT_LOC=$(/usr/bin/dirname $0)
cd $SCRIPT_LOC

if [ ! -f ../settings.conf ]; then
        echo "* Please see settings.example.conf and create a settings.conf file."
        exit 1
else
        echo "* Loading config: $(pwd)/../settings.conf"
        source ../settings.conf || exit_on_error "Cannot source settings."
fi

# Get my aws region from this instance placement
ZONE=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
if [ -z "$ZONE" ]; then
	echo "[E] This is not an aws instance."
	exit 1
fi
AWS_REGION=${ZONE::-1}

THIS_INSTANCE=$(curl -s http://169.254.169.254/latest/meta-data/instance-id);

if [ ! -f ../drives.conf ]; then
	ATTACH2=$($AWS_BIN ec2 create-volume --volume-type=gp2 --size=50 --output text --region=$AWS_REGION --profile=$AWS_PROFILE --availability-zone=$ZONE)
	echo $ATTACH2
	VOL2=$(echo $ATTACH2 | cut -d $' ' -f 7)

	ATTACH1=$($AWS_BIN ec2 create-volume --volume-type=gp2 --size=10 --output text --region=$AWS_REGION --profile=$AWS_PROFILE --availability-zone=$ZONE)
	echo $ATTACH1
	VOL1=$(echo $ATTACH1 | cut -d $' ' -f 7)

	echo "* Checking on status: $VOL1 / $VOL2"

	while true; do
		STATUS1=$($AWS_BIN ec2 describe-volumes --output=text --profile=$AWS_PROFILE --region=$AWS_REGION  --volume-ids=$VOL1 | grep -oh available)
		STATUS2=$($AWS_BIN ec2 describe-volumes --output=text --profile=$AWS_PROFILE --region=$AWS_REGION  --volume-ids=$VOL2 | grep -oh available)

		echo "Waiting: $STATUS1 / $STATUS2"

		if [ "$STATUS1" = "available" -a "$STATUS2" = "available" ]; then 
			echo "xx STATUS1: $STATUS1 STATUS2: $STATUS2"
			break;
		fi
		sleep 1
	done

	echo "Attach1: $STATUS1  Attach2: $STATUS2"

	ATTACH_STATUS1=$($AWS_BIN ec2 attach-volume --region=$AWS_REGION --profile=$AWS_PROFILE --output text --volume-id=$VOL1 --instance-id=$THIS_INSTANCE --device /dev/sdb)
	ATTACH_STATUS2=$($AWS_BIN ec2 attach-volume --region=$AWS_REGION --profile=$AWS_PROFILE --output text --volume-id=$VOL2 --instance-id=$THIS_INSTANCE --device /dev/sdc)

	echo "/dev/sdb=$VOL1" > ../drives.conf
	echo "/dev/sdc=$VOL2" >> ../drives.conf

	sleep 10
fi

# Get a list of drives and their settings
DRIVES=$(fdisk -l | grep -v 'ram' | grep -e "Disk /dev/" | sed -rn 's/Disk\ (.*?):/\1/p' | sort -h)
OLDIFS=$IFS
IFS=$'\n'
for DRIVEDATA in $DRIVES; do
        DRIVE=$(echo $DRIVEDATA | cut -d ' ' -f 1)
        GIGS=$(echo $DRIVEDATA | cut -d ' ' -f 2)
        BYTES=$(echo $DRIVEDATA | cut -d ' ' -f 4)
        HASPART=$(partprobe -s -d $DRIVE | sed -rn 's/partitions 1(.*)//p')

        echo "$DRIVE // $GIGS // $HASPART"
        # If we don't have partition - then make one
        if [ -z $HASPART ]; then
                echo "* Partitioning ${DRIVE} :: ${GIGS} GIG => ${BYTES} bytes"
                parted -s ${DRIVE} -- mklabel msdos \ mkpart primary xfs 1 -1s
                sync
		sleep 1
                PART=$(lsblk -l "${DRIVE}" | grep -e "part $" | cut -d$' ' -f 1)
                echo "* Formatting $PART xfs"
                mkfs.xfs /dev/$PART
                sync

                if [ -z $ADRIVE ]; then
			echo "*Adding /dev/${PART} to fstab"
			echo "/dev/${PART}      /var/www/       xfs     defaults,discard        0 0" >> /etc/fstab
                        ADRIVE=$DRIVE
                        ASIZE=$BYTES
			continue
                fi

                if [ -z $BDRIVE ]; then
			echo "*Adding /dev/${PART} to fstab"
			echo "/dev/${PART}      /var/lib/mysql/  xfs     defaults,discard,noatime        0 0" >> /etc/fstab
                        BDRIVE=$DRIVE
                        BSIZE=$BYTES
                fi
        fi

done


if [ -d /var/www ] ; then 
	echo "* Move webroot dirs"
	mv /var/www/ /var/www-old/
fi

mkdir -p /var/www/
mount -t xfs /var/www/
sync
chown -R ubuntu:ubuntu /var/www

if [ -d /var/lib/mysql ]; then
	echo "* Move mysql to  /var/lib/mysql-old"
	mv /var/lib/mysql /var/lib/mysql-old
fi

mkdir -p /var/lib/mysql
mount -t xfs /var/lib/mysql
sync
chown -R mysql:mysql /var/lib/mysql
cp -a /var/lib/mysql-old/* /var/lib/mysql/

echo "* I think we're done"
