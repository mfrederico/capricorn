#!/bin/bash

export PHP_VER=8.1
export PHP_LISTENPORT="90${PHP_VER//./}"


if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

if [ -z "$WEBUSER" ]; then
    echo "Please run sudo WEBUSER=yourwebuser $0"
    exit 1
fi

# If not latest ubuntu
#echo "* Initiate ondrej ppa"
#add-apt-repository -y ppa:ondrej/php
#apt-get -y update
# apt -y purge php-*
wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q > composer-install.php
php composer-install.php --install-dir /usr/bin --filename composer
apt -y autoremove && apt -y autoclean

apt-get -y --fix-broken install php$PHP_VER-fpm php$PHP_VER-soap php$PHP_VER-mbstring php$PHP_VER-curl php$PHP_VER-mysql php$PHP_VER-gd  php$PHP_VER-xml php$PHP_VER-zip php-imagick php-redis php-dompdf pkg-php-tools php$PHP_VER-bcmath
apt-get -y --fix-broken install php$PHP_VER-mcrypt 
 
echo "* Updating www.conf php fpm to listen at: $PHP_LISTENPORT"
if [ ! -e "/etc/php/$PHP_VER/fpm/pool.d/www.conf" ]; then
	echo "***** DID NOT INSTALL PHP $PHP_VER" 
else 
	CPUS=$(/usr/bin/nproc)
	NUMCHILD=$((CPUS*15))
	START=$((NUMCHILD / 2))
	MINSPARE=$((START / 2))
	MAXSPARE=$((NUMCHILD))

	/bin/sed -i -e "s/www-data/$WEBUSER/" -e "s/\/run\/php\/php$PHP_VER-fpm.sock/$PHP_LISTENPORT/" /etc/php/$PHP_VER/fpm/pool.d/www.conf
	/bin/sed -i -e "s/;opcache.enable=1/opcache.enable=1/" -e "s/memory_limit = 128M/memory_limit = 256M/" /etc/php/$PHP_VER/fpm/php.ini
	/bin/sed -i -e 's/^pm\.max_children.*=.*/pm.max_children='$NUMCHILD'/' /etc/php/$PHP_VER/fpm/pool.d/www.conf; 
	/bin/sed -i -e 's/^pm\.start_servers.*=.*/pm.start_servers='$START'/' /etc/php/$PHP_VER/fpm/pool.d/www.conf; 
	/bin/sed -i -e 's/^pm\.min_spare_servers.*=.*/pm.min_spare_servers='$MINSPARE'/' /etc/php/$PHP_VER/fpm/pool.d/www.conf; 
	/bin/sed -i -e 's/^pm\.max_spare_servers.*=.*/pm.max_spare_servers='$MAXSPARE'/' /etc/php/$PHP_VER/fpm/pool.d/www.conf; 

	echo "* Restarting php fpm"
	service php$PHP_VER-fpm restart

	echo "* Setting up upstreams"
	echo "upstream production { server 127.0.0.1:$PHP_LISTENPORT; }" > /etc/openresty/upstreams.conf
fi
