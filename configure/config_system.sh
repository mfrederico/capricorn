#!/bin/bash

CPUS=$(/usr/bin/nproc)
VER=2.0

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Voodoo to grab my settings
SCRIPT_LOC=$(/usr/bin/dirname $0)
cd $SCRIPT_LOC

if [ ! -f ../settings.conf ]; then
        echo "* Please see settings.example.conf and create a settings.conf file."
        exit 1
else
        echo "* Loading config: $(pwd)/../settings.conf"
        source ../settings.conf || exit_on_error "Cannot source settings."
fi

echo "* APT UPDATE"
apt-get update

echo "* Pulling in debconf utils"
apt-get install -y debconf-utils
apt-get install -y git
apt-get install -y ufw
apt-get install -y curl
apt-get install -y ncdu

# Get my aws region from this instance placement
ZONE=$(curl --connect-timeout 1 -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
if [ -z "$ZONE" ]; then
    rm -f /tmp/rsyncing
    echo "[E] I'm going to go ahead and say - this is probably not AWS .. "
else
	echo "* Get your aws secret and access keys ready from: https://console.aws.amazon.com/iam/home?region=$ZONE"
	apt-get install -y awscli
	/usr/bin/aws configure	
fi


echo "* Adding known-hosts for github and bitbucket for root"
if [ ! -d "/root/.ssh" ];
then
	mkdir -p /root/.ssh
fi

echo "* Setting up HA socket sysctl config"
# cat files/sysctl.conf >> /etc/sysctl.conf
# sysctl -p /etc/sysctl.conf

echo "* SSH keys for $WEBUSER"
touch /root/.ssh/known_hosts && ssh-keyscan -H github.com >> /root/.ssh/known_hosts && chmod 600 /root/.ssh/known_hosts
touch /root/.ssh/known_hosts && ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts && chmod 600 /root/.ssh/known_hosts

echo "* SED s/ubuntu/$WEBUSER/g"
sed -i -e "s/ubuntu/$WEBUSER/g" $(fgrep -lIr 'ubuntu' ../*);

mkdir -p /home/$WEBUSER/.ssh/
cp ../etc/dotssh/* /home/$WEBUSER/.ssh/
cat ../etc/dotssh/id_rsa.pub >> /home/$WEBUSER/.ssh/authorized_keys 
chmod 600 /home/$WEBUSER/.ssh/*

echo "* Installing Galera / MariaDB 10.2"
source ./install-database.sh

echo "* Installing PHPFPM"
source ./install-php.sh

echo "* Installing letsencrypt"
source ./install-letsencrypt.sh

echo "* Installing OpenResty"
source ./install-openresty.sh

echo "* Making magic 'm' in ~/bin/"
mkdir /home/$WEBUSER/bin
echo "mysql -u root -p$MYSQL_PASS \$@" > /home/$WEBUSER/bin/m
chmod +x ~/bin/m

echo "* Allowing specific sudo commands for xpi [/etc/sudoers.d/capricorn]"
usermod -a -G sudo $WEBUSER
echo "%sudo   ALL=(ALL:ALL) NOPASSWD:/usr/sbin/ufw" >> /etc/sudoers.d/capricorn
echo "%sudo   ALL=(ALL:ALL) NOPASSWD:/home/$WEBUSER/bin/m" >> /etc/sudoers.d/capricorn

echo "* Setting up web folders"
mkdir -p /var/www/html/default/
mkdir -p /var/www/storage/

echo "* Grab NODE" 
apt-get install -y nodejs nodejs-legacy npm

echo "* Adding web files sync from master"
sudo -u $WEBUSER cat ../cron/capricorn-sync | crontab 

echo "* Chowning dirs"
chown -R $WEBUSER:$WEBUSER /home/$WEBUSER/
chown -R $WEBUSER:$WEBUSER /etc/letsencrypt
chown -R $WEBUSER:$WEBUSER /var/www/
mkdir -p /var/log/nginx/
chown -R $WEBUSER:$WEBUSER /var/log/nginx/

echo "CAPRICORN $VER COMPLETE" > "/var/www/html/default/index.php"

PUBIP=$(curl --connect-timeout 3 -s http://checkip.amazonaws.com)
COMP=$(curl --connect-timeout 1 -s http://$PUBIP/wtf.php)


echo "**********************************"
echo "SERVER STATUS: $COMP"
echo "**********************************"
echo "* Your public IP is   : $PUBIP"
echo "* MySQL root password : $MYSQL_PASS"
echo "**********************************"
echo "* Visit http://$PUBIP/wtf.php"
echo ""
echo "Thank you for listening."
