#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

if [ -z $1 ]; then
   echo "Please specify a php version"
   exit 1
fi

sudo apt -y install unixodbc-dev
sudo apt -y install php-dev
sudo pecl install pdo_sqlsrv
sudo pecl install sqlsrv
printf "; priority=20\nextension=sqlsrv.so\n" > /etc/php/$1/mods-available/sqlsrv.ini
printf "; priority=30\nextension=pdo_sqlsrv.so\n" > /etc/php/$1/mods-available/pdo_sqlsrv.ini
sudo phpenmod -v $1 sqlsrv pdo_sqlsrv

sudo curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
sudo curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
sudo apt-get -y update
sudo ACCEPT_EULA=Y apt-get -y install msodbcsql17
sudo ACCEPT_EULA=Y apt-get -y install mssql-tools
