#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

if [ -z "$WEBUSER" ]; then
    echo "Please run sudo WEBUSER=yourwebuser $0"
    exit 1
fi

echo "* Grabbing CERTBOT / letsencrypt"
sudo apt install python3-certbot-dns-route53

# apt-get -y install snapd
# snap install --beta --classic certbot
# snap set certbot trust-plugin-with-root=ok
# snap install --beta certbot-dns-cloudflare
# snap connect certbot:plugin certbot-dns-cloudflare
# snap install --beta certbot-dns-route53
# snap connect certbot:plugin certbot-dns-route53
# ln -s /snap/certbot/current/bin/certbot /usr/bin/certbot

echo "* PLEASE MAKE SURE TO CHOWN the letsencrypt dirs to your web user"
mkdir /var/lib/letsencrypt
mkdir /var/log/letsencrypt
mkdir -p /etc/letsencrypt/live/

ln -s /home/$WEBUSER/tools/etc/ssl/default /etc/letsencrypt/live/default

chown -R $WEBUSER:$WEBUSER /var/lib/letsencrypt
chown -R $WEBUSER:$WEBUSER /var/log/letsencrypt
chown -R $WEBUSER:$WEBUSER /etc/letsencrypt
