#!/bin/bash
# sudo chown -R ubuntu:ubuntu /etc/letsencrypt /var/run/letsencrypt /var/log/letsencrypt/
if [[ -d /etc/letsencrypt/live/default || -L /etc/letsencrypt/live/default ]] ; then
        echo "* Running certbot"
        /usr/bin/certbot -n certonly --webroot -w /var/www/letsencrypt -d $@ 
else
        echo "* Symlinking default"
        mkdir -p /etc/letsencrypt/live/
		ln -s ./etc/ssl/default /etc/letsencrypt/live/default

        sleep 2
        $0 $1
fi

