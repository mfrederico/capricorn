#!/bin/bash
# Run this periodically to delete stale binlog files for wsrep

DAYSAGO=1
if [ -n "$1" ]; then 
	DAYSAGO=$1; 
else 
	DAYSAGO="1"; 
fi
LOG_DATE=$(date "+%Y-%m-%d" -d "-$DAYSAGO days")
echo "PURGE BINARY LOGS BEFORE '$LOG_DATE 00:00:00'" | ~/bin/m

