#!/bin/bash

# Voodoo to grab my settings
SCRIPT_LOC=$(/usr/bin/dirname $0)
cd $SCRIPT_LOC

if [ ! -f ../settings.conf ]; then
        echo "* Please see settings.example.conf and create a settings.conf file."
        exit 1
else
        echo "* Loading config: $(pwd)/../settings.conf"
        source ../settings.conf || exit_on_error "Cannot source settings."
fi

# Get my aws region from this instance placement
ZONE=$(curl -m 2 -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
if [ -z "$ZONE" ]; then
		rm -f /tmp/rsyncing
		echo "[E] I'm going to go ahead and say - this is probably not AWS .. "
		exit 1
fi
AWS_REGION=${ZONE::-1}

if [ ! -f /tmp/rsyncing ]; then
        touch /tmp/rsyncing
        echo "-------------------------"
        echo $(date)

        AWSCMD="$AWS_BIN --output text --region=$AWS_REGION --profile=$AWS_PROFILE"
        echo "AWS CMD: $AWSCMD"

        MYIP=$(curl -m 2 -s http://169.254.169.254/latest/meta-data/local-ipv4)

        chown $WEBUSER:$WEBUSER /home/$WEBUSER/.ssh/authorized_keys

        DISPOSITION=$($AWSCMD ec2 describe-tags --query 'Tags[?Key==`Disposition`]' | grep instance | grep $SOURCETAG)

        SOURCEINSTANCE=$(echo $DISPOSITION | cut -d ' ' -f 2)

        SOURCEIP=$($AWSCMD ec2 describe-instances --query "Reservations[*].Instances[*].{Ip:PrivateIpAddress,Inst:InstanceId}" | grep $SOURCEINSTANCE | cut -f 2)

        echo "Source Instance : $SOURCEINSTANCE"
        echo "Source IP4      : $SOURCEIP"

		sed -i ../settings.conf -e "s/MYSQL_MASTER=.*/MYSQL_MASTER=$SOURCEIP/"

        if [ -z $SOURCEIP ]; then
                rm -f /tmp/rsyncing
                echo "No source web content available."
                exit 1
        fi
        if [ "$SOURCEIP" == "$MYIP" ]; then
                rm -f /tmp/rsyncing
                echo "I am the source - aborting"
                exit 1
        fi

        if [ ! -d /var/www/html/core-ds/ ]; then
                mkdir -p /var/www/html/core-ds/
        fi


        /usr/bin/rsync -alvzC --chown=$WEBUSER:$WEBUSER --delete -e "ssh -o StrictHostKeyChecking=no -o ConnectTimeout=30" $WEBUSER@$SOURCEIP:/etc/letsencrypt/* /etc/letsencrypt/

        /usr/bin/rsync -alvzC --chown=$WEBUSER:$WEBUSER --delete --exclude="debug.log" --exclude='/var/www/html/storage/*' --exclude='storage/logs/*' --exclude='storage/framework/cache/*' --exclude='storage/framework/views/*' -e "ssh -o StrictHostKeyChecking=no -o ConnectTimeout=30" $WEBUSER@$SOURCEIP:/var/www/html/ /var/www/html/

        rm -f /tmp/rsyncing

        CORE='/var/www/html/core-ds/';
        FIELD='APP_STORAGE'

        cd $CORE

        for f_p in $( fgrep -r $FIELD .env* ); do
                DOTENV=$(echo $f_p | cut -d ":" -f 1 | cut -b 6-)
                SPATH=$(echo $f_p | cut -d ":" -f 2)
                export $SPATH;

                if [ ! -d ${!FIELD} ]; then
                        echo "MAKING FOLDERS: ${!FIELD}"
                        mkdir -p ${!FIELD}
                        chown -R $WEBUSER:$WEBUSER /var/www/storage/*
                        sudo -u $WEBUSER php artisan storage:mkdir --env=${DOTENV}
                fi
        done
else
        echo "* Sync currently in progress."
fi;
