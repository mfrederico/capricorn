#REPLICANTS=$(echo "show status like 'wsrep_incoming%'" | ~/bin/m -N | sed -e 's/wsrep_incoming_addresses\W//' -e 's/:3306//g' -e 's/,/\n/g')

REPLICANTS=$(maxctrl show servers --tsv | grep -e '^Address' | cut -f 2 | sed -e 's/[^0-9.]*//g')

for R in $REPLICANTS; do
	echo "Server: ${R} -------------------------------------"
	ssh -o ConnectTimeout=1 -t ${R} $@
	echo "END: ${R} -------------------------------------"
done
