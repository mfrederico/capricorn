#!/bin/bash

# Voodoo to grab my settings
SCRIPT_LOC=$(/usr/bin/dirname $0)
cd $SCRIPT_LOC

if [ ! -f ../settings.conf ]; then
        exit 1
else
        source ../settings.conf || exit_on_error "Cannot source settings."
fi

ZONE=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
AWS_REGION=${ZONE::-1}

# Set up command to use for AWS
AWSCMD="$AWS_BIN --output text --region=$AWS_REGION --profile=$AWS_PROFILE"
SRCINSTID=$($AWSCMD ec2 describe-tags --query 'Tags[?Key==`Disposition`]' --filters "Name=resource-type,Values=instance" | grep $SOURCETAG | cut -f 2)
SRCIP=$($AWSCMD ec2 describe-instances --instance-id $SRCINSTID | grep PRIVATEIPADDRESS | cut -f 4)

echo "$SRCIP"
