UN="root"
PW="build4n0w"
mysqldump="mysqldump -u $UN -p$PW --events --quick --single-transaction"
exclude="information_schema|performance_schema"

cd /tmp

# Backup and re-import each database on the system
for db in $(mysql -u $UN -p$PW -e "show databases;" -s --skip-column-names | grep -vE "($exclude)")
do
        echo "Repairing database $db"
        $mysqldump $db > $db.sql
        mysql -u $UN -p$PW $db < $db.sql
done
