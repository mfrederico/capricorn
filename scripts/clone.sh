#!/bin/bash

# Voodoo to grab my settings
SCRIPT_LOC=$(/usr/bin/dirname $0)
cd $SCRIPT_LOC

if [ ! -f ../settings.conf ]; then
        echo "* Please see settings.example.conf and create a settings.conf file."
        exit 1
else
        echo "* Loading config: $(pwd)/../settings.conf"
        source ../settings.conf || exit_on_error "Cannot source settings."
fi

if [ ! -f /tmp/rsyncing ]; then
        touch /tmp/rsyncing
        echo "-------------------------"
        echo $(date)

		# Get my aws region from this instance placement
		ZONE=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
        if [ -z "$ZONE" ]; then
                rm -f /tmp/rsyncing
                echo "[E] I'm going to go ahead and say - this is probably not AWS .. "
                exit 1
        fi

		AWS_REGION=${ZONE::-1}

		# Set up command to use for AWS
		AWSCMD="$AWS_BIN --output text --region=$AWS_REGION --profile=$AWS_PROFILE"

		# Get parent / Source ip information from this instance Parent tag
		PARENT=$($AWSCMD ec2 describe-tags --query 'Tags[?Key==`Parent`]' --filters "Name=resource-type,Values=instance" | grep $(curl -s http://169.254.169.254/latest/meta-data/instance-id) | cut -f 4)

        if [ -z "$PARENT" ]; then
                rm -f /tmp/rsyncing
                echo "[E] No parent tag specified to source sync from"
                exit 1
        fi

		SRCINSTID=$($AWSCMD ec2 describe-tags --query 'Tags[?Key==`Disposition`]' --filters "Name=resource-type,Values=instance" | grep $PARENT | cut -f 2)
		SRCIP=$($AWSCMD ec2 describe-instances --instance-id $SRCINSTID | grep PRIVATEIPADDRESS | cut -f 4)
        MYIP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

        chown $WEBUSER:$WEBUSER /home/$WEBUSER/.ssh/authorized_keys

        echo "Source Instance : $SRCINSTID"
        echo "Source IP4      : $SRCIP"

        if [ -z $SRCIP ]; then
                rm -f /tmp/rsyncing
                echo "[W] No source web server available."
                exit 1
        fi
        if [ "$SRCIP" == "$MYIP" ]; then
                rm -f /tmp/rsyncing
                echo "[W] I am the source - aborting"
                exit 1
        fi

        if [ ! -d /var/www/html/core-ds/ ]; then
                mkdir -p /var/www/html/core-ds/
        fi

		# This could possibly throw an error if using shsfs to clone tools dir
		sed -i ../settings.conf -e "s/MYSQL_MASTER=.*/MYSQL_MASTER=$SRCIP/"

		echo "- Syncing Certificates"
        /usr/bin/rsync -alvzC --chown=$WEBUSER:$WEBUSER --delete -e "ssh -o StrictHostKeyChecking=no -o ConnectTimeout=30" $WEBUSER@$SRCIP:/etc/letsencrypt/* /etc/letsencrypt/

		echo "- Syncing webroot folders"
        /usr/bin/rsync -alvzC --chown=$WEBUSER:$WEBUSER --delete --exclude="debug.log" --exclude='/var/www/html/storage/*' --exclude='storage/logs/*' --exclude='storage/framework/cache/*' --exclude='storage/framework/views/*' -e "ssh -o StrictHostKeyChecking=no -o ConnectTimeout=30" $WEBUSER@$SRCIP:/var/www/html/ /var/www/html/

		echo "- Syncing storage folder structure"
		/usr/bin/rsync -alvzC -f"+ */" -f"- *" -e ssh "$WEBUSER@$SRCIP:/var/www/storage/" /var/www/storage/

		echo "- Syncing /etc/nginx/*"
		touch /tmp/nginx-log

        /usr/bin/rsync --log-file=/tmp/nginx-log -alvzC --exclude="upstreams.conf" --chown=$WEBUSER:$WEBUSER -e "ssh -o StrictHostKeyChecking=no -o ConnectTimeout=30" $WEBUSER@$SRCIP:/etc/nginx/* /etc/nginx/

		RESTARTNGINX=$(wc -l /tmp/nginx-log | cut -d' ' -f 1)
		if [ $RESTARTNGINX -gt 3 ]; then
			echo "* Config changes detected - Reloading nginx"
			rm -f /tmp/nginx-log
			sudo service nginx reload
		fi

        rm -f /tmp/rsyncing
else
        echo "* Sync currently in progress."
fi;
