for fwnum in $(ufw status numbered | grep DENY | grep -Po '\[\K[^]]*' | sort -nr); do
	echo  "Firewall Num: $fwnum"
	yes | ufw delete $fwnum --force
done
ufw reload
