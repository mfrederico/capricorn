#!/bin/bash
RESTY=/usr/sbin/nginx
PIDFILE=/var/run/nginx_rtmp.pid

start_it_up() {
	echo "* Starting"
	ufw allow 1935
	$RESTY -c /home/ubuntu/tools/services/rtmp/rtmp.conf
}

shut_it_down() {
	echo "* Stopping"
	ufw deny 1935
	kill -SIGINT $(cat $PIDFILE)
}

reload_it() {
	echo "* Reloading"
	kill -HUP $(cat $PIDFILE)
}

case "$1" in
  start)
    start_it_up
  ;;
  stop)
    shut_it_down
  ;;
  reload|force-reload)
    reload_it
  ;;
  restart)
    shut_it_down
    start_it_up
  ;;
  *)
    echo "Usage: $0 {start|stop|reload|restart|force-reload|status}" >&2
    exit 2
  ;;
esac
