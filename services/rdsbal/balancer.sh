#!/bin/bash
RESTY=/usr/share/nginx_balance/nginx/sbin/nginx
PIDFILE=/var/run/nginx_balancer.pid

start_it_up() {
	echo "* Starting"
	$RESTY -c /home/ubuntu/tools/balancer/nginx.conf
}

shut_it_down() {
	echo "* Stopping"
	kill -SIGINT $(cat $PIDFILE)
}

reload_it() {
	echo "* Reloading"
	kill -HUP $(cat $PIDFILE)
}

case "$1" in
  start)
    start_it_up
  ;;
  stop)
    shut_it_down
  ;;
  reload|force-reload)
    reload_it
  ;;
  restart)
    shut_it_down
    start_it_up
  ;;
  *)
    echo "Usage: $0 {start|stop|reload|restart|force-reload|status}" >&2
    exit 2
  ;;
esac
