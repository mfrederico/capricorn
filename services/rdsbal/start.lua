--[[
Nginx Log Levels
   ngx.STDERR
   ngx.EMERG
   ngx.ALERT
   ngx.CRIT
   ngx.ERR
   ngx.WARN
   ngx.NOTICE
   ngx.INFO
   ngx.DEBUG
]]
-- Useful libs
local cjson         = require 'cjson'
local balancer  	= require "ngx.balancer"
local func			= require 'cpad.functions';

-- Shared Dicts
local threshtime	= ngx.shared.threshtime;
local connect   	= ngx.shared.connections;

-- Can only use ip addresses at this time
local defaulthost   = '127.0.0.1';

function connectTo(host) -- ip.add.re.ss:port
	local stats         = require 'stats'

	 -- My indexing hash for shared dict
	local connectinfo = host:split(':');

	-- Split into host / port
	local hostip	= connectinfo[1];
	local port		= connectinfo[2];


	-- Set my connection!
	local ok, err = balancer.set_current_peer(hostip, port)
	if not ok then
		ngx.log(ngx.ERR, "failed to set the current peer ["..host.."] : ", err)
		return false;
	else 
		local newval, err   = connect:incr(host,1, 0);
		local total, err    = threshtime:incr('tcount', 1);

		-- Write my stats file for this host
		stats:writeHostStatsFile(''..host, newval, total);
	    ngx.log(ngx.ALERT,'HOST: '..host..' N: '..newval..' T: '..total);
	end

	-- Set my timeouts
	local ok, err = balancer.set_timeouts(10,60,60);
	if not ok then
		ngx.log(ngx.WARN, "Could not set timeouts: ", err)
	end

end

-- Least connected / round robin style
function findLeastConnected() 
	--local connect		= ngx.shared.connections;
	--local threshtime	= ngx.shared.threshtime;
	local lastConn	= 0;
	local lastHost  = '';
	local tthresh	= 0;

	for idx, host in ipairs(connect:get_keys(200)) do
		local connhost      = host;
		local val,err		= connect:get(connhost);

		-- Break out if we have a server with 0 connections
		if (val == 0) then 
			lastConn	= val; 
			lastHost	= host;
			break;
		end

		-- First go around we set current lastConn
		if (lastConn == 0) then 
			lastConn	= val; 
			lastHost	= host;
		end

		ngx.log(ngx.INFO,'CONNCOMP: '..lastConn..' >= '..val..' IDX: '..idx..' HOST: '..connhost);

		if (val <= lastConn) then
			lastConn = val;
			lastHost  = host;
		end	

		ngx.log(ngx.INFO,'LOOK: '..connhost..' CONN: '..val.. ' TTHRESH: '..threshtime:get('tcount'));
	end

	-- ngx.log(ngx.WARN,'LEASTCONN: '..lastHost);
	return lastHost;
end

-- Using the maindbymysql as the replicant
local happy			= connectTo(findLeastConnected());
