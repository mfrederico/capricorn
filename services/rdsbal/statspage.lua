local lfs = require("lfs");
local statsPath = "/etc/nginx/balancer/stats/";
local statsFile = statsPath.."general.json";

local cjson     = require 'cjson'
local content   = {};
local sfile     = '';

local file      = io.open(statsFile, "r");
local general   = cjson.decode(file:read("*all"));
file:close();

for sfile in lfs.dir(statsPath) do
	if (sfile ~= '..' and sfile ~= '.' and sfile ~= 'general.json') then
		local file,err          = io.open(statsPath..sfile, "r");

		if (file ~= nil) then
			local jhost,err = sfile.gsub(sfile, '.json','');
			local jhostdata = file:read("*all");
			file:close();

			if (jhostdata ~= '') then
				content[jhost]  = cjson.decode(jhostdata);
			else 
				ngx.log(ngx.ERR,jhost..': Not available.');
			end

		else
			ngx.log(ngx.ERR,'File cannot be read '..statsPath..sfile..': '..err);
		end

	end
end

content['general'] = general;

ngx.say(cjson.encode(content));

