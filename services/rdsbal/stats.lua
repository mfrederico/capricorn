module("functions", package.seeall)

local func = {}

local lfs		= require("lfs");
local envvars	= ngx.shared.envvars;
-- local cjson     = require 'cjson'

function func:writeGeneralStatsFile(statsData)
    local statsFile = envvars:get('STATSPATH')..'general.json';
    local file,err = io.open(statsFile, "w+");
    if (file ~= nil) then
        file:write(cjson.encode(statsData));
        file:close();
    else
        ngx.log(ngx.ERR, 'Can not write statsfile '..statsFile..': '..err);
    end
end

function func:writeHostStatsFile(hostname, connect, thresh);
    local hostStatsFile = envvars:get('STATSPATH')..hostname..'.json';
    local file,err = io.open(hostStatsFile,'w+');

    if (file ~= nil) then
        file:write('{"connections":'..connect..', "tcount":'..thresh..', "last":'..math.ceil(ngx.now())..'}');
        file:close();
    else
        ngx.log(ngx.ERR, 'Can not write hostStatsFile: '..err);
    end
end

return func;
