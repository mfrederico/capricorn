local stats     = require "stats"

local connect   = ngx.shared.connections;
local threshtime = ngx.shared.threshtime;

local tcount, err           = threshtime:get('tcount');
local last_time, err        = threshtime:get('last_time')

-- My indexing hash for shared dict
local connhost      = ngx.var.upstream_addr;

if (connhost ~= nil) then
	local newval, err   = connect:incr(connhost,-1);
	local totals, err   = threshtime:incr('tcount',-1);
	stats:writeHostStatsFile(connhost, newval, totals);
	-- ngx.log(ngx.ERR,"DISCONNECTED "..connhost.." : "..newval);
end

