local envvars		= ngx.shared.envvars;

local connect       = ngx.shared.connections;
local threshtime    = ngx.shared.threshtime;
local instances		= ngx.shared.instanceData;

threshtime:set('last_time', 0);
threshtime:set('tcount', 0);

cjson     			= require 'cjson'
func				= require 'cpad.functions';
stats				= require('stats');


function loadInstanceFile(instanceFile, replicant)
    local file,err     = io.open(instanceFile, "r");
    local cnt       = 0;
	local ifdcnt	= 0;
	local instanceFileData = {};

    if (file ~= nil) then
        for line in file:lines() do
            local linedata = line:split(',');
            -- If this is replicant of the correct "replicant" classification
            if ((linedata[1] == replicant)) then
				instanceFileData[ifdcnt] = linedata;
				ifdcnt = ifdcnt + 1;

				-- Get only 'available' instances
				if (linedata[4] == 'available') then
					connect:add(linedata[2],0,0);
					cnt = cnt + 1;
				else 
					connect:delete(linedata[2]);
				end
            end
        end
		file:close();
        return instanceFileData;
    else
		ngx.log(ngx.ERR,'Cannot find instance file: '..err);
        return false;
    end
end

function turnDown(replicant)
	local allinstances = loadInstanceFile(envvars:get('INSTANCELIST'), envvars:get('REPLICANT'));

	for idx, instance in pairs(allinstances) do
		ngx.log(ngx.ERR, 'Replicant: '..instance[1]..' Ip/port: '..instance[2]..' Identifier: '..instance[3].. ' Status: '..instance[4]);

		-- If we have no connections on this turnDownable instance 
		-- turnDownable is one auto-created here and not manually in AWS
		if (connect:get(instance[2]) == 0 and instance[4] == 'available' and instances:get(instance[3]) ~= nil) then
			lbIdentifier = instance[3];

			-- Have cron come in and delete them after specified zerotime minutes
			command ='/usr/bin/aws rds delete-db-instance --region '..envvars:get('REGION')..' --profile controlpad --skip-final-snapshot --db-instance-identifier '..lbIdentifier;

			lbstatus = io.open('/tmp/'..lbIdentifier..'.json','a');
			lbstatus:write(command.."\n");

			instances:delete(lbIdentifier);

			ngx.log(ngx.ERR, 'TURN DOWN: '..lbIdentifier);

			-- If we're in debug mode then dryrun
			if (envvars:get('DEBUG') == '1') then
				ngx.log(ngx.ERR, 'DRYRUN: '..command);
			else
				ngx.log(ngx.ERR, 'REALRUN: '..command);
				local handle = io.popen(command.." 2>&1")
				if (handle) then
					local line = assert(handle:read('*a'))
					lbstatus:write(line);
				end

				lbstatus:close();
			end

			-- We only want to do drop one at a time as per a fallof threshold
			break; -- Remove this if you want them ALL to falloff immediately
		end
	end

end

function pumpUp(sourceReplica)
	local hostCount		= 0;
	local idCount		= 0;


	local allinstances	= loadInstanceFile(envvars:get('INSTANCELIST'), envvars:get('REPLICANT'));

	local lbIdentifier	= 'UNAVAILABLE';
	local cheating		= {};
	local repid			= '';

	-- Count of connectablehost
	for idx, host in ipairs(connect:get_keys(200)) do
		hostCount	= idx;
	end

	-- Determine the next available lb identifier up to MAX_SERVERS
	for idx, instance in pairs(allinstances) do
		cheating[instance[3]] = 1;
	end

	for i=hostCount, envvars:get('MAX_SERVERS'),1 do
		repid = 'lb-'..sourceReplica..'-'..i;
		-- IF we haven't already spun it up
		if (cheating[repid] == nil and instances:get(repid) == nil) then
			lbIdentifier = repid;
			break;
		end
	end
	
	ngx.log(ngx.ERR,'IDENTIFIER: '..lbIdentifier);

	if (lbIdentifier == 'UNAVAILABLE') then
		ngx.log(ngx.ERR,'MAXIMUIM CAPACITY: '..envvars:get('MAX_SERVERS'));
	else
		local command = '/usr/bin/aws rds create-db-instance-read-replica --region '..envvars:get('REGION')..' --profile controlpad --db-instance-identifier '..lbIdentifier..' --source-db-instance-identifier '..sourceReplica..' --db-instance-class '..envvars:get('INSTANCECLASS')..' --no-publicly-accessible --availability-zone '..envvars:get('AVAILABILITY_ZONE');

		lbstatus = io.open('/tmp/'..lbIdentifier..'.json','a');
		lbstatus:write(command.."\n");

		instances:set(lbIdentifier, math.ceil(ngx.now()));

		ngx.log(ngx.ERR, 'TURN UP: '..lbIdentifier);

		-- If we're in debug mode then dryrun
		if (envvars:get('DEBUG') == '1') then
			ngx.log(ngx.ERR,'DRYRUN: '..command);
		else
			ngx.log(ngx.ERR,'REALRUN: '..command);
			local handle = io.popen(command.." 2>&1")
			if (handle) then
				local line = assert(handle:read('*a'))
				lbstatus:write(line);
			end
				lbstatus:close();
		end

		ngx.log(ngx.ERR, 'PUMPED UP: /tmp/'..lbIdentifier..'.json');
	end

end



-- Monitor our thresholds
monitorThresh = function(premature)
	if (premature) then 
		ngx.log(ngx.ERR,' PREMATURE EJECTULIFICATION');
		return 
	end

	local connect       		= ngx.shared.connections;
	local threshtime    		= ngx.shared.threshtime;
	local totalActive			= 0;
	local hostCount				= 1;
	local idCount		= 0;

	local tcount, err       	= threshtime:get('tcount');
	local last_time, err       	= threshtime:get('last_time');
	local replData				= {};

	-- Every little CPU cycle is precious
	local nowtime = math.ceil(ngx.now());

	loadInstanceFile(envvars:get('INSTANCELIST'), envvars:get('REPLICANT'));

	for idx, host in ipairs(connect:get_keys(200)) do
		totalActive = totalActive + connect:get(host);	
		hostCount	= idx;
	end


	-- Get Krunk
	if (hostCount * tonumber(envvars:get('CONNECT_THRESHOLD')) < tcount) then
		threshtime:set('last_time',nowtime);
		-- Write a file that my crontab checks 
		ngx.log(ngx.ERR, '********* '..(hostCount * tonumber(envvars:get('CONNECT_THRESHOLD')))..' < '..tcount .. ' PUMP UP THE VOLUME: ************');
		local co = ngx.thread.spawn(pumpUp, envvars:get('REPLICANT'))
	end 

	-- Wait for my last time I was krunk
	if (
		(last_time > 0) and 
		(tcount < tonumber(envvars:get('CONNECT_THRESHOLD'))) and 
		(nowtime - last_time > tonumber(envvars:get('COOLDOWN_TIME')))) then

		threshtime:set('last_time', nowtime);
		ngx.log(ngx.ERR, '********* '..(nowtime - last_time)..' > '..envvars:get('COOLDOWN_TIME').. ' TURN DOWN FOR WHAT! ************');

		for idx, host in ipairs(connect:get_keys(200)) do
			totalActive = totalActive + connect:get(host);	
			hostCount	= idx;
		end

		if (hostCount > 1) then 
			local co = ngx.thread.spawn(turnDown, envvars:get('REPLICANT'))
		else
			ngx.log(ngx.ERR, 'Minimum host requirement of 1');
		end

	end

	ngx.log(ngx.ERR,'['..envvars:get('REPLICANT')..'] Total Active: '..totalActive..' Hosts: '..hostCount..' TCount: '..tcount .. ' Cooldown Time: '..math.ceil(tonumber(envvars:get('COOLDOWN_TIME')) - (nowtime - last_time)));

	replData['replicant']	= envvars:get('REPLICANT');
	replData['totalActive'] = totalActive;
	replData['hostCount']	= hostCount;
	replData['tcount']		= tcount;
	replData['cooldown']	= nowtime - last_time;
	replData['cooldown_thresh']	= tonumber(envvars:get('COOLDOWN_TIME'));
	replData['time']		= nowtime;
	replData['lastTime']	= last_time;

	stats:writeGeneralStatsFile(replData)

end

func.loadSharedSettings('/home/ubuntu/tools/balancer/settings.conf', envvars);

ngx.log(ngx.ERR,'SETTINGS: '.. cjson.encode(envvars:get_keys(200)));
loadInstanceFile(envvars:get('INSTANCELIST'), envvars:get('REPLICANT'))

-- Set up my timer to check health generally every 5 seconds
ngx.timer.every(5, monitorThresh);
