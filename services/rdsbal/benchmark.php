<?php

$MYSQLLB	= '52.23.187.202';	// Server
$MYSQLLBPT	= '3306'; 		// Server PORT
$MYSQLU		= 'root';
$MYSQLP		= 'build4n0w';
$MYSQLDB	= 'piphanydev_prod';

$CONCURRENT	= 100;	// How many processes to run
$SLEEP		= 1;	// delay between query rounds
$ROUNDS		= 10;	// How many times to execute the query
$SLOW   	= .7;   // These get dumped to slow file

// File to load queries from
$QF			= "queries.sql";
$QUERIES    = explode(';',file_get_contents($QF));

$numq		= count($QUERIES);

if (isset($argv[1])) $CONCURRENT = $argv[1];
if (isset($argv[2])) $SLEEP = $argv[2];
unlink('/tmp/success.test');
unlink('/tmp/slow.test');

for ($i = 0; $i <= $CONCURRENT; $i++) {
	print "Opening: {$i} running: {$numq} queries\n";
    $pid = pcntl_fork();
    if (!$pid) {
        $DATE = date('Y-m-d H:i:s');
        $mysqli = mysqli_connect($MYSQLLB, $MYSQLU, $MYSQLP, $MYSQLDB, $MYSQLLBPT);
		$PID = getmypid();
		if ($mysqli) {
			if ($mysqli->connect_errno) {
				file_put_contents('/tmp/failures.test',"[$DATE] Failed to connect to MySQL: (" . $mysqli->connect_errno . ") {$mysqli->connect_error} \n", FILE_APPEND);
			}
			else {
				for($x =0; $x < $ROUNDS;$x++) { 
					print "ROUND: {$x} - FIGHT!\r";
					foreach($QUERIES as $qidx=>$QUERY) { 
						$affrows	= 0;
						$ts			= microtime(true);
						$plist		= $mysqli->query($QUERY);
						$te			= microtime(true);

						if (is_bool($plist)) $affrows = mysqli_affected_rows($mysqli);
						else $affrows = mysqli_num_rows($plist);

						$diff = $te - $ts;
						if ($diff < $SLOW) { 
							file_put_contents('/tmp/success.test',"[{$PID}] [{$DATE}] IDX: {$qidx} TIME: [{$diff}]  ROWS: {$affrows} -> Errors: ".json_encode($mysqli->error)."\n", FILE_APPEND);
						} else {
							file_put_contents('/tmp/slow.test',"[{$PID}] [{$DATE}] IDX: {$qidx} TIME: [{$diff}]  ROWS: {$affrows} -> Errors: ".json_encode($mysqli->error)."\n", FILE_APPEND);
						}
					}
					sleep($SLEEP);
				}
			}
		}
		else
		{
			file_put_contents('/tmp/failures.test',"[$DATE] Failed to connect to MySQL: (" . $mysqli->connect_errno . ") {$mysqli->connect_error} \n", FILE_APPEND);
		}

		$mysqli->close();
		exit;
    }
}

print "\nWaiting: \n";

while (pcntl_waitpid(0, $status) != -1) {
    $status = pcntl_wexitstatus($status);
    echo "Child $status completed\n";
}

