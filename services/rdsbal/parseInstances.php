<?php

$data = json_decode(`/usr/bin/aws rds --region us-west-1 --profile controlpad describe-db-instances`);
// /usr/bin/aws rds create-db-instance-read-replica --region us-west-1 --profile controlpad --db-instance-identifier prodbalancer --source-db-instance-identifier piphanyprod --db-instance-class db.t2.large --no-publicly-accessible --availability-zone us-west-1b

foreach($data->DBInstances as $instance) {
        if (isset($instance->Endpoint) && isset($instance->ReadReplicaSourceDBInstanceIdentifier)){
                print "{$instance->ReadReplicaSourceDBInstanceIdentifier},".gethostbyname($instance->Endpoint->Address).":{$instance->Endpoint->Port},{$instance->DBInstanceIdentifier},{$instance->DBInstanceStatus}\n";
        }
}

