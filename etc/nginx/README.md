### For LetsEncrypt ###
sudo chown -R www-data:www-data /etc/letsencrypt
sudo chown -R www-data:www-data /var/lib/letsencrypt
sudo chown -R www-data:www-data /var/log/letsencrypt*

#### For openresty ###
./configure --prefix=/usr/share/nginx --conf-path=/etc/nginx/nginx.conf --http-log-path=/var/log/nginx/access.log --error-log-path=/var/log/nginx/error.log --lock-path=/var/lock/nginx.lock --pid-path=/run/nginx.pid --http-client-body-temp-path=/var/lib/nginx/body --http-fastcgi-temp-path=/var/lib/nginx/fastcgi --http-proxy-temp-path=/var/lib/nginx/proxy --http-scgi-temp-path=/var/lib/nginx/scgi --http-uwsgi-temp-path=/var/lib/nginx/uwsgi --with-debug --with-pcre-jit --with-ipv6 --with-http_geoip_module --build=MJF_Controlpad

#### FOR WSO2 ####
find * | grep site.json
edit those files: vim $(find * | grep site.json)
set the reverseProxy to "auto"
