ngx.header['Content-Type'] = 'application/json';
local status = { status = 'error' } 
local cjson		= require "cjson"

local function check_ssl_cert(domain)
	-- make sure the file exists
	source_fname = '/etc/letsencrypt/live/'..domain..'/fullchain.pem'
	ngx.log(ngx.INFO, source_fname)
	local file = io.open(source_fname)
	if not file then
	  return false;
	end
	file:close()
	return true;
end

if (ngx.var.arg_lookup) then
	local resolver	= require "resty.dns.resolver"

	-- local status = {}
	status.status = 'error';

	local r, err = resolver:new{
		nameservers = {"127.0.0.1"},
		retrans = 5,  -- 5 retransmissions on receive timeout
		timeout = 2000,  -- 2 sec
	}

	if not r then
		status.status = 'error';
		status.type		= 'dnsfail'
		status.message = "failed to instantiate the resolver: ";
		status.err = err;
		ngx.say(cjson.encode(status));
		return
	end

	local originserver, err	= r:query(ngx.var.host);		-- Look myself up
	local answers, err 		= r:query(ngx.var.arg_lookup);  -- Look up the lookup dns

	if not originserver or not answers then
		status.message = "failed to query DNS server";
		status.type		= 'dnsfail'
		ngx.say(cjson.encode(status));
		return
	elseif originserver.errcode then
		status.message = "Origin Server DNS Error Code: "..originserver.errcode;
		status.type		= 'dnsfail'
		status.server  = originserver;
		ngx.say(cjson.encode(status));
		return
	elseif answers.errcode then
		status.message = "DNS Error Code: "..answers.errcode;
		status.type		= 'dnsfail'
		status.server  = answers;
		ngx.say(cjson.encode(status));
		return
	end

	local dnsmatchstatus	= 'dns-notfound';
	local certstatus		= 'cert-notfound';

	local hascert			= 0;
	local hasdns			= 0;

	for i, ans in ipairs(answers) do
		dnsmatchstatus	= 'dns-pending';
		if (ans.address == originserver[1].address) then
			dnsmatchstatus	= 'dns-success';
			break
		end
	end
	if (check_ssl_cert(ngx.var.arg_lookup)) then 
		certstatus = 'cert-success';
	end
	status.SSL		= certstatus;
	status.DNS		= dnsmatchstatus;
	status.arecord	= originserver[1].address;
	status.cname	= originserver[1].name;
	status.status	= 'success';
	ngx.say(cjson.encode(status));
else 
	status.type		= 'inputfail'
	status.message = 'Please specify a lookup name.';
	ngx.say(cjson.encode(status));
end
