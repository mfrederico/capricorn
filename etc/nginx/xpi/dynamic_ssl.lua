-- # see: https://certbot.eff.org/#ubuntutyakkety-nginx

-- # Make sure the letsencrypt PEMkeys are readable by www-data
-- # DO: sudo chown www-data /etc/letsencrypt/archive/ .. and logs .. and configs .. etc.

-- # This responds with the appropriate ssl cert for SNI responses

local myfunc = require 'cpad.functions';
local cjson	 = require "cjson";
local ssl = require "ngx.ssl"

	-- clear the fallback certificates and private keys
	-- set by the ssl_certificate and ssl_certificate_key
	-- directives above:
	local ok, err = ssl.clear_certs()
	if (not ok) then
		ngx.log(ngx.ERR, "failed to clear existing (fallback) certificates")
		return ngx.exit(ngx.ERROR)
	end

	-- assuming the user already defines the my_load_certificate_chain()
	-- herself.

	local pem_cert_chain = assert(myfunc.my_load_certificate_chain(ssl))
	-- assert(my_load_certificate_chain())

	local der_cert_chain, err = ssl.cert_pem_to_der(pem_cert_chain)
	if (not der_cert_chain) then
		ngx.log(ngx.ERR, "failed to convert certificate chain ",
				"from PEM to DER: ", err)
		return ngx.exit(ngx.ERROR)
	end

	local ok, err = ssl.set_der_cert(der_cert_chain)
	if (not ok) then
		ngx.log(ngx.ERR, "failed to set DER cert: ", err)
		return ngx.exit(ngx.ERROR)
	end

	-- assuming the user already defines the my_load_private_key()
	-- function herself.
	local pem_pkey = assert(myfunc.my_load_private_key(ssl))

	local der_pkey, err = ssl.priv_key_pem_to_der(pem_pkey)
	if (not der_pkey) then
		ngx.log(ngx.ERR, "failed to convert private key ",
				"from PEM to DER: ", err)
		return ngx.exit(ngx.ERROR)
	end

	local ok, err = ssl.set_der_priv_key(der_pkey)
	if (not ok) then
		ngx.log(ngx.ERR, "failed to set DER private key: ", err)
		return ngx.exit(ngx.ERROR)
	end
