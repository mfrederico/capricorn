local myfunc		= require('cpad/functions');
local cjson     	= require "cjson"

---------------------------
-- Get my proxy env info --
---------------------------
if (ngx.var.sname == nil) then ngx.var.sname = 'localhost'; end
local proxyfile = '/var/www/html/.proxy.'..ngx.var.sname;

local proxydata = myfunc.loadEnvFile(proxyfile);

if (proxydata) then
	ngx.log(ngx.INFO, '*******************************************');
	ngx.log(ngx.ERR, 'PROXYFILE   : '..cjson.encode(proxydata));
	return proxydata;
end

return false
