local cjson		= require "cjson"

ngx.header['Content-Type'] = 'application/json';
local status = {}
status.status = 'error'
status.message = 'unabled to complete functions'

local function check_ssl_cert(domain)
	-- make sure the file exists
	source_fname = '/etc/letsencrypt/live/'..domain..'/fullchain.pem'
	ngx.log(ngx.DEBUG, source_fname)
	local file = io.open(source_fname)
	if not file then
	  return false;
	end
	file:close()
	return true;
end

function readAll(file)
    local f = io.open(file, "r")
    local length = 0

	if (f) then
		local content = f:read("*all")
		f:close()
		return content
	else
		return false;
	end
end

if (ngx.var.arg_lookup) then

	-- First lets check to see if we have SSL or CERT
	local res = ngx.location.capture("/dnsstatus?lookup="..ngx.var.arg_lookup)

	local dnsstatus = cjson.decode(res.body);

	status.status = 'failure';

	if (dnsstatus.DNS == 'dns-pending') then
		status.type		= 'dnsfail';
		status.message	= 'You will need to point your DNS name A record to: '..dnsstatus.arecord..
						 ' OR CNAME to: '..dnsstatus.cname
	elseif (dnsstatus.SSL == 'cert-success') then
		status.type		= 'certfail';
		status.message	= 'This domain already has a certificate issued: '..dnsstatus.SSL;
	else
		status.domain	= ngx.var.arg_lookup;
	 	status.executed	= os.execute('/home/ubuntu/tools/scripts/runcertbot.sh '..ngx.var.arg_lookup);
		status.botsays	= readAll('/tmp/'..ngx.var.arg_lookup);

		local res = ngx.location.capture("/dnsstatus?lookup="..ngx.var.arg_lookup)
		local dnsstatus = cjson.decode(res.body);
		if (dnsstatus.SSL == 'cert-success') then
			status.message = 'Certificate issued';
			status.status = 'success';
		else 
			status.type = 'issuefail';
			status.message = 'Certificate issuance failure';
		end
	end
	
else 
	status.status = 'error';
	status.message = 'please specify a domain name';
end

-- This is my final status message output
ngx.say(cjson.encode(status));
