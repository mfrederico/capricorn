local SECOND	= 5; -- Window of time to keep request count for
local connect   = ngx.shared.connections;
local connhost  = ngx.var.http_x_forwarded_for;

if (connhost ~= nil) then
	ngx.log(ngx.INFO, 'Forwarded ip incremented: '..connhost);
else
	connhost  = ngx.var.remote_addr;
	ngx.log(ngx.INFO, 'Direct ip incremented: '..connhost);
end

-- Reset our expiration 
if (connect:get(connhost) ~= nil)
then
	-- Increment our ip counter
	local newval, err   = connect:incr(connhost,1,1);
else
	-- Flush expired and start anew
	connect:flush_expired();
	local succ, err = connect:set(connhost, 1, SECOND)
end

if (connect.expire ~= nil) then
	connect:expire(connhost, SECOND)
end

ngx.log(ngx.INFO, 'TTL '..SECOND..' seconds: '..connhost.. ' count: '..connect:get(connhost));
