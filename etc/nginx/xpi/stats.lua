require 'resty.core';
local resty_md5		= require "resty.md5"
local str			= require "resty.string"
local cjson			= require "cjson"
local session		= require "ngx/ssl/session"
local lfs			= require "lfs"
local auth			= require 'cpad.authentication';
local myfunc		= require 'cpad.functions';
local mysql			= require 'resty.mysql';

local FPATH="/home/ubuntu/bin:/home/ubuntu/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

local configjson,err = ngx.location.capture("/xpi/getenv/DB_READHOST,DB_PASSWORD,DB_USERNAME,DB_PORT,DB_DATABASE,APP_URL");
local conf = cjson.decode(configjson.body);

local datestart = ngx.var.arg_start or ngx.today()..' 00:00:00' 
local dateend	= ngx.var.arg_end or ngx.today();

local LIMIT		= '10';
local ORDER		= 'DESC';
local BETWEEN   = "BETWEEN "..datestart.." AND "..dateend

local coreEnv = ngx.var.new_root..'../.env'
local envfile = coreEnv;

-- If we actually found an env file
if (ngx.var.envfile ~= 'NOTFOUND') then
    envfile = coreEnv..'.'..ngx.var.envfile;
end


-- Minor sanity check
if (ngx.var.arg_order ~= nil) then
	ORDER = string.upper(ngx.var.arg_order);
	if (ORDER ~= 'DESC' and ORDER ~= 'ASC') then ORDER='DESC' end
end

if (ngx.var.arg_limit ~= nil) then 
	LIMIT = tonumber(ngx.var.arg_limit);
end

local req = ngx.req.get_method()
ngx.header['Content-Type'] = 'text/plain';
ngx.header['Access-Control-Allow-Methods']		= 'GET, POST, PATCH, PUT, DELETE, OPTIONS';
ngx.header['Access-Control-Allow-Headers']		= 'Origin, Content-Type, X-Auth-Token, authorization';
ngx.header['Access-Control-Allow-Credentials']	= 'true';
ngx.header['Access-Control-Allow-Origin'] 		= '*';

if (req == 'OPTIONS') then
	ngx.say('Yay for preflight asspain!');
	ngx.exit(ngx.OK);
end

function parseDate(date)
	local loc,ok = string.find(date,'-');
	if (ok == nil) then
		local year	= string.sub(date,1,4); -- year
		local month = string.sub(date,5,-3); -- month
		local day	= string.sub(date,7,-1); -- day
		return year..'-'..month..'-'..day
	else 
		return date;
	end
end

datestart	= ngx.quote_sql_str(parseDate(datestart));
dateend		= ngx.quote_sql_str(parseDate(dateend)..' 23:59:59');
BETWEEN   = "BETWEEN "..datestart.." AND "..dateend

--------------------------------------------------------------
-- This actually runs the sql and generates the report data --
--------------------------------------------------------------
function doQuery(conf, REPORTQ)
	-- Connect to the database
	local db = mysql:new();
	local rows = {};
	local ok, err, errcode, sqlstate = db:connect({
		host = conf.DB_READHOST,
		port = conf.DB_PORT,
		database = conf.DB_DATABASE,
		user = conf.DB_USERNAME,
		password = conf.DB_PASSWORD})

	db:query("SET time_zone = 'MST'");
	db:query("SET sql_mode = ''");

	if (ok) then
		local res, err, errcode, sqlstate = db:query(REPORTQ);
		if (err ~= nil) then
			return false
		end
		rows = res;
	end

	return rows;
end

function pivotOn(res, piv) 
	local pivot = {};
	
	for idx, pk in pairs(piv) do
		local finres = {}

		if type(pk) == 'table' then 
			pivot[#pivot + 1] = pivotOn(res, pk)
		else 
			for i,row in pairs(res) do 
				finres[i] = row[pk];
			end
			pivot[idx] = finres;
		end
	end

	return pivot;
end

function doReport(conf, reportname, REPORTQ, FieldOrder, Pivot)
	if reportname == nil then reportname = 'report'; end
	local res = doQuery(conf, REPORTQ);

	ngx.header['Content-Type'] = 'text/plain';

	local headers = {};

	-- Set my headers for field ordering
	if (FieldOrder ~= nil) then 
		headers = FieldOrder
	else
		for k,v in pairs(res) do 
			for h,vv in pairs(v) do
				headers[#headers+1] = h;
			end
			break;
		end
	end

	if (Pivot ~= nil ) then
		res = pivotOn(res, Pivot)
		headers = res[1];
		res = res[2];
		ngx.say(myfunc.toCSV(headers));
		for k,v in pairs(res) do 
			ngx.say(myfunc.toCSV(v));
		end
		return res;
	else 
		ngx.say(myfunc.toCSV(headers));
		local allout = {}

		-- Now get the rest of everything
		for k,v in pairs(res) do 
			local line = {};
			for _,hk in pairs(headers) do
				line[#line+1] = v[hk];
			end

			table.insert(allout, line)
			ngx.say(myfunc.toCSV(line));
		end
		return allout
	end
	
end

if (ngx.var.arg_owner ~= nil) then 
	owner_id = ngx.var.arg_owner 
else 
	owner_id = 1;
end

if (ngx.var.arg_whoami) then
	local who = {};
	who.is_internal = ngx.req.is_internal();
	who.connecting_addr = ngx.var.remote_addr;
	if (ngx.var.http_x_forwarded_for ~= nil) then
		who.remote_addr = ngx.var.http_x_forwarded_for;
	end
	ngx.say(cjson.encode(who));
	ngx.exit(ngx.OK);

elseif (ngx.var.arg_get == 'pacing') then
    auth.HTTP(nil, envfile);
-- Recency / Freq Metric
    local REPORTQ="select DATE_FORMAT(created_at,'%Y-%m') as `Date`,SUM(total_price) as `Total($)` FROM orders WHERE DATE_FORMAT(created_at,'%d') < DATE_FORMAT(NOW(),'%d')  GROUP BY `Date` DESC LIMIT 2"

    local x = doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Date' , 'Total($)' }, { 'Date', { 'Total($)' }});
	ngx.say('Labels,T1($),T2($)');
	ngx.say('Target,'..x[1][2]);
	ngx.say('LabelShow,1');

elseif (ngx.var.arg_get == 'repdanger') then
    auth.HTTP(nil, envfile);
-- Recency / Freq Metric
    local REPORTQ="SELECT DATEDIFF(MAX(orders.updated_at), users.updated_at) AS `Recency`,CONCAT('#',users.id) AS `Id`, CONCAT(users.first_name,' ',users.last_name) AS `Customer`,COUNT(*) AS `Orders`, SUM(orders.total_price) AS `Value($)`, (SELECT SUM(total_price) FROM orders WHERE store_owner_user_id=users.id) as `Retail($)` FROM orders LEFT JOIN users on (orders.customer_id = users.id) WHERE users.status='active' and users.role_id=5 GROUP BY `Customer` HAVING `Recency` > 30 ORDER BY `Recency` DESC"

    doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Recency','Id','Customer','Orders','Value($)' });

elseif (ngx.var.arg_get == 'retailsales') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT DATE_FORMAT(orders.created_at,'%Y-%m-%d') as `Date`, SUM(total_price) as `Total($)` FROM orders WHERE store_owner_user_id > 1 AND orders.created_at "..BETWEEN.." GROUP BY `Date` ASC"

	local x = doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Date', 'Total($)' });
elseif (ngx.var.arg_get == 'avgspanandsize') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT COUNT(DISTINCT(users.id)) as `Reps`, ROUND(AVG(DATEDIFF(NOW(),orders.created_at)),1) AS `Order Span`,ROUND(AVG(orders.total_price)) AS `Avg Order($)` FROM users LEFT JOIN orders ON (orders.customer_id=users.id) WHERE orders.created_at "..BETWEEN.." AND users.status='active' AND users.role_id=5"

	local x = doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Reps', 'Order Span', 'Avg Order($)' });


elseif (ngx.var.arg_get == 'avgagetop50') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT ROUND(AVG(DATEDIFF(users.created_at,now())),1) users.created_at BETWEEN "..datestart.." AND "..dateend;

	local x = doReport(conf,'Inventory_'..owner_id, REPORTQ, { '(#)', 'Rep', 'Orders', 'Total($)' });


elseif (ngx.var.arg_get == 'top50wholesale') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT CONCAT('#',orders.customer_id) AS `(#)`,CONCAT(first_name,' ',last_name) AS `Rep`, COUNT(*) AS `Orders`, SUM(total_price) as `Total($)` FROM orders LEFT JOIN users on (orders.customer_id=users.id) WHERE store_owner_user_id='1' AND orders.created_at BETWEEN "..datestart.." AND "..dateend.." GROUP BY orders.customer_id HAVING `Total($)` > 1 ORDER BY `Total($)` DESC LIMIT 50"

	local x = doReport(conf,'Inventory_'..owner_id, REPORTQ, { '(#)', 'Rep', 'Orders', 'Total($)' });


elseif (ngx.var.arg_get == 'top50sellers') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT CONCAT('#',store_owner_user_id) AS `(#)`,CONCAT(first_name,' ',last_name) AS `Rep`, COUNT(*) as `Orders`, sum(total_price) as `Total($)` FROM orders LEFT JOIN users on (orders.store_owner_user_id=users.id) WHERE store_owner_user_id > 1 AND orders.created_at BETWEEN "..datestart.." and "..dateend.." GROUP BY store_owner_user_id HAVING `Total($)` > 1 ORDER BY `Total($)` DESC LIMIT 50"

	local x = doReport(conf,'Inventory_'..owner_id, REPORTQ, { '(#)', 'Rep', 'Orders', 'Total($)' });

elseif (ngx.var.arg_get == 'totalcustomers') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT COUNT(DISTINCT(users.id)) AS `Retail`, EXP(AVG(LOG(orders.total_price))) as `Value($)` FROM users LEFT JOIN orders ON (users.id=orders.customer_id) WHERE users.role_id=3";

	doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Retail' ,'Value($)'});

elseif (ngx.var.arg_get == 'totalactive') then 
    auth.HTTP(nil, envfile);
	-- local REPORTQ="SELECT SUM(CASE users.status WHEN 'active' THEN 1 ELSE 0 END) as `Active`, SUM(CASE users.status WHEN 'active' THEN 0 ELSE 1 END) as `Disabled` FROM users WHERE users.role_id=5;"
	-- local REPORTQ="SELECT SUM(CASE users.status WHEN 'active' THEN 1 ELSE 0 END) as `Active`, SUM(CASE users.status WHEN 'active' THEN 0 ELSE 1 END) as `Disabled` , (SELECT EXP(AVG(LOG(orders.total_price))) FROM orders left join users on(orders.customer_id=users.id) WHERE  users.role_id=5 and users.status='active') AS `RepValue($)` FROM users WHERE users.role_id=5"
	local REPORTQ="SELECT COUNT(DISTINCT(users.id)) AS `Active`, EXP(AVG(LOG(orders.total_price))) as `Value($)` FROM users LEFT JOIN orders ON (users.id=orders.customer_id) WHERE users.role_id=5 AND users.status='active'";

	doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Active','Value($)' });

elseif (ngx.var.arg_get == 'activevsnon') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT DATE_FORMAT(CASE users.status WHEN 'active' THEN users.created_at ELSE users.updated_at END,'%Y-%m-%d') as `Date`, SUM(CASE users.status WHEN 'active' THEN 1 ELSE 0 END) as `Activated`, SUM(CASE users.status WHEN 'active' THEN 0 ELSE 1 END) as `Disabled` FROM users WHERE users.role_id=5 GROUP BY `Date` HAVING `Date` BETWEEN "..datestart.." AND "..dateend.." "

	doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Date','Activated','Disabled' });

elseif (ngx.var.arg_get == 'repsignups') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT DATE_FORMAT(users.created_at,'%Y-%m-%d') AS `Date`, SUM(1) as `Signups` from users left join roles on (roles.id=users.role_id) WHERE users.created_at BETWEEN "..datestart.." AND "..dateend.." AND status IN('active') AND roles.name="..ngx.quote_sql_str(ngx.var.arg_role).." group by `Date` order by users.created_at ASC;"

	ngx.say(REPORTQ);
	-- doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Date','Signups' });

elseif (ngx.var.arg_get == 'allusers') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT users.id as `UserID`,email `Email`,concat(first_name,' ',last_name) `Name`,status `Status`,phone_number `Phone`,public_id `PublicID`,users.created_at `Created`,join_date `Joined`,sponsor_id `Sponsor` FROM users left join roles on (users.role_id=roles.id) WHERE roles.name='Rep' ORDER BY users.id ASC;"
	
	ngx.header['Content-Type'] = 'application/json';
	ngx.say(cjson.encode(doQuery(conf, REPORTQ)));

elseif (ngx.var.arg_get == 'rolestatusavglife') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT name AS `Role Status`,SUM(1) as `Num`, ROUND(AVG(DATEDIFF(users.updated_at, users.created_at)),1) as `Avg Lifetime` from users left join roles on (roles.id=users.role_id) WHERE users.updated_at BETWEEN "..datestart.." AND "..dateend.." AND status NOT IN('active') group by roles.name,status order by users.updated_at ASC;"
	
	doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Role Status','Num','Avg Lifetime'},  { 'Role Status', {'Avg Lifetime'}});

elseif (ngx.var.arg_get == 'rolestatus') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT name AS `Role Status`,SUM(1) as `Num`, ROUND(AVG(DATEDIFF(users.updated_at, users.created_at)),1) as `Avg Lifetime` from users left join roles on (roles.id=users.role_id) WHERE users.updated_at BETWEEN "..datestart.." AND "..dateend.." AND status IN('active') group by roles.name,status order by users.updated_at ASC;"
	
	doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Role Status','Num','Avg Lifetime'},  { 'Role Status', {'Num'}});

elseif (ngx.var.arg_get == 'salesbyuser') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT DATE_FORMAT(orders.created_at,'%Y-%m-%d') as `Date`, SUM(total_price) AS `Revenue($)`, SUM(ABS(total_discount)) as `Discount($)`, COUNT(*) `Orders` FROM orders WHERE orders.created_at BETWEEN "..datestart.." AND "..dateend.." AND store_owner_user_id="..ngx.quote_sql_str(owner_id).." GROUP BY `Date` DESC"

	doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Date','Revenue($)','Discount($)','Orders'} );
elseif (ngx.var.arg_get == 'shiplag_gauge_H') then 
    auth.HTTP(nil, envfile);
	-- Gets my order to ship rate for today
	local REPORTQ1="SELECT DATE_FORMAT(orders.created_at,'%Y-%m-%d') as `Date`, ROUND(AVG((UNIX_TIMESTAMP(tracking.shipped_at) - UNIX_TIMESTAMP(orders.created_at)) / 3600),1) as 'Hours', COUNT(DISTINCT(orders.id)) AS `Orders`, (SUM(orderlines.quantity) / COUNT(DISTINCT(orders.id))) AS `Avg Items Picked` FROM tracking left join orders on orders.id=order_id LEFT JOIN orderlines ON orderlines.order_id=orders.id WHERE orders.created_at BETWEEN "..datestart.." AND "..dateend.." AND orders.store_owner_user_id="..ngx.quote_sql_str(owner_id).." GROUP BY `Date` ORDER BY `Date` DESC LIMIT 1"

	-- Gets my overall average order to ship rate for today
	--local REPORTQ2="SELECT ROUND(AVG((UNIX_TIMESTAMP(tracking.shipped_at) - UNIX_TIMESTAMP(orders.created_at)) / 3600),1) as 'Hours', COUNT(DISTINCT(orders.id)),count(1), (SUM(orderlines.quantity) / COUNT(DISTINCT(orders.id))) AS `Avg Shipped` FROM tracking left join orders on orders.id=order_id LEFT JOIN orderlines ON orderlines.order_id=orders.id WHERE tracking.created_at BETWEEN "..datestart.." AND "..dateend.." AND orders.store_owner_user_id="..ngx.quote_sql_str(owner_id)
	local REPORTQ2="SELECT ROUND(AVG((UNIX_TIMESTAMP(tracking.shipped_at) - UNIX_TIMESTAMP(orders.created_at)) / 3600),1) as 'Hours', COUNT(DISTINCT(orders.id)),count(1), (SUM(orderlines.quantity) / COUNT(DISTINCT(orders.id))) AS `Avg Shipped` FROM tracking left join orders on orders.id=order_id LEFT JOIN orderlines ON orderlines.order_id=orders.id WHERE orders.store_owner_user_id="..ngx.quote_sql_str(owner_id)

	local x1 = doQuery(conf, REPORTQ1, { 'Hours' , 'Orders' });
	local x2 = doQuery(conf, REPORTQ2, { 'Hours' , 'Orders' });

	ngx.say(myfunc.toCSV( { 'Hours','Target' } ));
	ngx.say(myfunc.toCSV( { x1[1].Hours, x2[1].Hours })) ;
	ngx.say(myfunc.toCSV({ 'Reverse','0','1' }));
	ngx.say(myfunc.toCSV({ 'Color','#ff0000' }));
	ngx.say('LabelShow,1');


elseif (ngx.var.arg_get == 'ordertypes') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="select order_types.name AS `Types`, SUM(orders.total_price) AS `Amount($)` from orders left join order_types on (orders.type_id=order_types.id) WHERE orders.created_at BETWEEN "..datestart.." AND "..dateend.." group by `Types`"

	doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Size', 'Value' }, { 'Types', { 'Amount($)' }});


elseif (ngx.var.arg_get == 'shiplag') then 
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT DATE_FORMAT(orders.created_at,'%Y-%m-%d') as `Date`, ROUND(AVG((UNIX_TIMESTAMP(tracking.shipped_at) - UNIX_TIMESTAMP(orders.created_at)) / 3600),1) as 'Hours', COUNT(DISTINCT(orders.id)) AS `Orders`, (SUM(orderlines.quantity) / COUNT(DISTINCT(orders.id))) AS `Avg Items Picked` FROM tracking left join orders on orders.id=order_id LEFT JOIN orderlines ON orderlines.order_id=orders.id WHERE orders.created_at BETWEEN "..datestart.." AND "..dateend.." AND orders.store_owner_user_id="..ngx.quote_sql_str(owner_id).." GROUP BY `Date` ORDER BY `Date` ASC"

	doReport(conf,'Sales_'..owner_id, REPORTQ, { 'Date','Hours','Orders','Avg Items Picked' });

	ngx.say(myfunc.toCSV({ 'Reverse','1','0','0' }));
	ngx.say(myfunc.toCSV({ 'Average','1','1','1' }));
	ngx.say(myfunc.toCSV({ 'Color','#ff0000','#00FF00','#0000FF' }));

elseif (ngx.var.arg_get == 'salesbyitem') then 
    auth.HTTP(nil, envfile);
	if (ngx.var.arg_owner ~= nil) then 
		owner_id = ngx.var.arg_owner 
	else 
		owner_id = 1;
	end
	local REPORTQ="SELECT date_format(orderlines.created_at, '%Y-%m-%d') as 'Reporting Date', orderlines.name as 'Item Name', orderlines.manufacturer_sku as 'Item SKU', items.size as 'Item Size', IF (items.print IS NULL, 'N/A', items.print) as 'Item Print', SUM(orderlines.quantity) AS 'Quantity Sold', orderlines.price as 'Unit Price', SUM(orderlines.price*orderlines.quantity) AS 'Total Amount' FROM orderlines LEFT JOIN items on (items.id=orderlines.item_id) WHERE orderlines.inventory_owner_id IN("..ngx.quote_sql_str(owner_id)..") AND orderlines.created_at BETWEEN "..datestart.." AND "..dateend.." group by orderlines.manufacturer_sku ORDER BY orderlines.created_at DESC;"

	doReport(conf,'Sales_'..owner_id, REPORTQ);
elseif (ngx.var.arg_get == 'inventorytop20') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT date_format(orderlines.created_at, '%Y-%m-%d') as `Date`, CONCAT(items.size,' ', orderlines.name) as 'Item', orderlines.manufacturer_sku as 'SKU', SUM(orderlines.quantity * orderlines.price) AS `Value` FROM orderlines LEFT JOIN items on (items.id=orderlines.item_id) WHERE orderlines.inventory_owner_id IN("..ngx.quote_sql_str(owner_id)..") AND orderlines.created_at BETWEEN "..datestart.." and "..dateend.." GROUP BY orderlines.manufacturer_sku ORDER BY `Date`,`Value` DESC LIMIT 20"

	doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Item','SKU','Value' });

elseif (ngx.var.arg_get == 'inventorysize') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT date_format(orderlines.created_at, '%Y-%m-%d') as `Date`, items.size as 'Size', SUM(orderlines.quantity * orderlines.price) AS `Value` FROM orderlines LEFT JOIN items on (items.id=orderlines.item_id) WHERE orderlines.inventory_owner_id IN("..ngx.quote_sql_str(owner_id)..") AND orderlines.created_at BETWEEN "..datestart.." AND "..dateend.." GROUP BY items.size ORDER BY `Date`,`Value`"

	doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Size', 'Value' }, { 'Size', { 'Value' }});
elseif (ngx.var.arg_get == 'inventoryproductsize') then
    auth.HTTP(nil, envfile);

	local REPORTQ="SELECT DISTINCT(CONCAT(products.name,' ', items.size)) as `Item NameSize`, SUM(orderlines.quantity) AS `Ordered` FROM inventories LEFT JOIN items ON (inventories.item_id=items.id) RIGHT JOIN products ON (items.product_id = products.id) LEFT JOIN orderlines ON (items.id=orderlines.item_id) WHERE inventories.quantity_available > 0 AND inventories.owner_id=1 GROUP BY `Item NameSize` HAVING `Ordered` > 0 ORDER BY `Ordered` "..ORDER.." LIMIT "..LIMIT;

	doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Item NameSize', 'Ordered' } , { 'Item NameSize', { 'Ordered' } });

elseif (ngx.var.arg_get == 'inventoryproduct') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT DISTINCT(products.name) as `Item Name`, SUM(orderlines.quantity) AS `Ordered` FROM inventories LEFT JOIN items ON (inventories.item_id=items.id) RIGHT JOIN products ON (items.product_id = products.id) LEFT JOIN orderlines ON (items.id=orderlines.item_id) WHERE inventories.quantity_available > 0 AND inventories.owner_id=1 GROUP BY `Item Name` HAVING `Ordered` > 0 ORDER BY `Ordered` DESC;"
	doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Item Name', 'Ordered' } , { 'Item Name', { 'Ordered' } });

elseif (ngx.var.arg_get == 'inventory') then
    auth.HTTP(nil, envfile);
	local REPORTQ="SELECT products.tax_class as 'Tax Class', products.name as 'Item Name', items.size as 'Item Size', IF (items.print IS NULL, 'N/A', items.print) as 'Item Print', items.manufacturer_sku as 'Item SKU', inventories.quantity_available as 'Available', inventories.quantity_staged as 'Staged' from inventories left join items on (inventories.item_id=items.id) left join products on (items.product_id = products.id) where inventories.owner_id="..ngx.quote_sql_str(owner_id);

	doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Tax Class','Item Name','Item Size','Item Print','Item SKU','Available','Staged'} );
elseif (ngx.var.arg_get == 'fullinventory') then
    auth.HTTP(nil, envfile);
    ngx.header['Content-Disposition'] =  'attachment; filename="Inventory_'..owner_id..'.csv"';

	if (ngx.var.arg_owner ~= nil) then 
		owner_id = ngx.var.arg_owner 
	else 
		owner_id = 1;
	end
    local REPORTQ="SELECT DATE_FORMAT(items.created_at,'%Y-%m-%d') as `Date` ,IF(ISNULL(products.deleted_at),'AVAILABLE','DEL') AS 'Status',products.tax_class AS 'Tax Class',items.manufacturer_sku AS 'Manufacturer SKU', products.name 'Product', items.print AS 'Print', items.size AS 'Size', inventories.quantity_available AS 'Inventory', prices.price AS 'Wholesale Price', inventories.quantity_available * prices.price AS 'Liability' from prices left join items on (priceable_id=items.id) left join products on (items.product_id=products.id) left join inventories on (items.id=item_id and owner_id="..ngx.quote_sql_str(owner_id)..") where priceable_type='App\\\\Models\\\\Item' and price_type_id=1 HAVING Status='AVAILABLE' order by products.name, owner_id"

	doReport(conf,'Inventory_'..owner_id, REPORTQ);

elseif (ngx.var.arg_get == 'inventorypacing') then
    auth.HTTP(nil, envfile);
    ngx.header['Content-Disposition'] =  'attachment; filename="Inventory_Pacing-'..owner_id..'-'..datestart..'-'..dateend..'.csv"';
	local REPORTQ="SELECT DATE_FORMAT(orderlines.created_at,'%Y-%m-%d') as `Date`, quantity_available as `Current Stock`, manufacturer_sku as `SKU`, sum(quantity) as `Num Sold` FROM orderlines LEFT JOIN inventories ON (inventories.item_id=orderlines.item_id) WHERE orderlines.created_at BETWEEN "..datestart.." AND "..dateend.." AND inventory_owner_id="..ngx.quote_sql_str(owner_id).." AND inventories.owner_id="..ngx.quote_sql_str(owner_id).." GROUP BY `Date`,orderlines.item_id;"

	doReport(conf,'Inventory_Pacing-'..owner_id, REPORTQ, {'Date','SKU','Current Stock','Num Sold'});
	-- doReport(conf,'Inventory_'..owner_id, REPORTQ, { 'Item NameSize', 'Ordered' } , { 'Item NameSize', { 'Ordered' } });
end

ngx.exit(ngx.OK);
