----------------------------------------------------------------------
-- Example: 
-- xapi/getenv/FACEBOOK_CALLBACK_URL,DB_READHOST,DB_WRITEHOST,DB_USERNAME,DB_PASSWORD

local cjson = require 'cjson';
local returnable = {}
local coreEnv = ngx.var.new_root..'../.env'
local envfile = coreEnv;

-- If we actually found an env file
if (ngx.var.envfile ~= 'NOTFOUND') then
	envfile = coreEnv..'.'..ngx.var.envfile;
end

local funcs  = require 'cpad.functions'
local authme = require 'cpad.authentication'

local a = authme.HTTP(nil, envfile);

-- Load the env file

local envvalues = funcs.loadEnvFile(envfile);
local getvars = funcs.splituri(ngx.var.uri);

if (getvars ~= '*' and envvalues ~= false) then
	for i,var in ipairs(getvars) do
		if (envvalues[var] ~= nil) then
			returnable[var] = envvalues[var];
			else returnable[var] = nil;
		end
	end
else
	returnable = envvalues;
end

ngx.header['Content-Type'] = 'application/json';
ngx.say(cjson.encode(returnable));
ngx.exit(ngx.OK);

