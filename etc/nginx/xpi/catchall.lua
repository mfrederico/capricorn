local query_string = '';
local cjson = require 'cjson';
local myfunc        = require('cpad/functions');
local ssldomain   = 'https://'..myfunc.parseHostname(ngx.var.host);


if (ngx.var.query_string) then
	query_string = '?'..ngx.var.query_string;
end

local redir   = ssldomain..ngx.var.uri..query_string;
local naked_ip = ngx.re.match(ngx.var.host,"^([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3})$");

ngx.log(ngx.DEBUG,'* DOMAIN: '..ssldomain)
ngx.log(ngx.DEBUG,'* NEKKID: '..cjson.encode(naked_ip))

-- This is for AWS to know Im alive
if (string.match(ngx.var.uri,'wtf.php')) then
	ngx.header['Content-Type'] = 'text/plain';
	if (ssldomain == 'https://.')then
		ssldomain = '';
	end
	ngx.say('Pong: '..ssldomain..' '..ngx.localtime());
	return
-- This is so we score better on PCI scanning
elseif (naked_ip ~= nil) then
	ngx.status=302
	ngx.log(ngx.DEBUG,'* NOTNIL NEKKID: '..cjson.encode(naked_ip))
--	ngx.header['Content-Type'] = 'text/plain';
	ngx.header['Location'] = 'https://cannonwms.com/xpi/deny/?ip='..ngx.var.remote_addr
	-- ngx.header['Location'] = 'https://clicksimple.com/?ip='..ngx.var.remote_addr
	ngx.say('GONE BABY GONE')
	-- Execute them
	-- ngx.exit(ngx.HTTP_CLOSE);
	return
else
	ngx.header['Content-Type'] = 'text/html';
	ngx.header['x-catchall-redir'] = 'yep';
	ngx.redirect(redir, ngx.HTTP_MOVED_TEMPORARILY);
	return
end
