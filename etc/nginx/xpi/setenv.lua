----------------------------------------------------------------------
-- Example: 
-- xapi/setenv/FACEBOOK_CALLBACK_URL=https://facebook.url.com
ngx.header['Content-Type'] = 'application/json';

local cjson = require 'cjson';
local returnable = {}
local coreEnv = ngx.var.new_root..'../.env'
local envfile = coreEnv;

-- If we actually found an env file
if (ngx.var.envfile ~= 'NOTFOUND') then
	envfile = coreEnv..'.'..ngx.var.envfile;
end

local authme = require 'cpad.authentication'
local funcs  = require 'cpad.functions'

local a = authme.HTTP(nil, envfile);

-- Load the env file
local envvalues = funcs.loadEnvFile(envfile);

-- Set nginx to read the body of the POST
ngx.req.read_body()
local args, err = ngx.req.get_uri_args()

-- upsert my values into my environment
for ak,av in pairs(args) do 
	ngx.say(ak);
	envvalues[ak] = av;
end
table.sort(envvalues);

-- Wite out my env file
local result = funcs.writeEnvFile(envfile, envvalues);

if (result ~= false) then
	ngx.say(cjson.encode(result));
	ngx.exit(ngx.OK);
else
	ngx.say(cjson.encode(false));
	ngx.exit(ngx.ERROR);
end
