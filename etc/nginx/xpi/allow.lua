---------------------------
-- mfrederico nginx xpi  -- 
-- Firewall settings 1.0 --
---------------------------
local UFW       = "sudo /usr/sbin/ufw"
local cjson     = require 'cjson';

-- The prefix for AWSBIN commands
local UFW_ALLOW     = UFW..' insert 1 allow from '..ngx.var.arg_ip..'/24  to any comment "'..os.date()..'"';
local jsonResponse  = {}

------------------------
-- Functions          --
------------------------
local function getCommandResults(command)
    ngx.log(ngx.INFO,command);
    local handle = io.popen(command.." 2>&1")
    local entries   = '';
    if (handle) then
        local line = assert(handle:read('*a'))
        entries = line;
        io.close(handle);
    end
    return entries;
end

--[[
allowme.sh: 
MYIP=$(curl -s checkip.amazonaws.com)
curl https://$1/xpi/allow/?ip=$MYIP
]]--

jsonResponse.message        = getCommandResults(UFW_ALLOW)
jsonResponse.status         = 'success';
ngx.header['Content-type'] = 'application/json';
ngx.say(cjson.encode(jsonResponse));
