-- TODO Check if core / wordpress / alternate
-- TODO write a microservice that detects the current envireonment and returns in json
-- TODO Check if swapfile exists npm is a monster for ram!!


local resty_md5 = require "resty.md5"
local str = require "resty.string"
local cjson		= require "cjson"
local session   = require "ngx/ssl/session"
local lfs = require "lfs"
local auth = require 'cpad.authentication';
local func = require 'cpad.functions';

local FPATH="/home/ubuntu/bin:/home/ubuntu/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

ngx.header['Content-Type'] = 'application/json';
local status = {}
status.status = 'error'
status.message = 'unabled to complete functions'

-- keeps us from setting a branch on a live server.
if (string.match(ngx.var.sname,'dev$') == nil and string.match(ngx.var.tld,'^local') == nil) then
	ngx.header['Content-Type'] = 'text/html';
	ngx.say('<h1>Invalid domain type</h1>');
	ngx.exit(ngx.OK);
end

if (ngx.var.arg_name) then
	ngx.header["Content-Type"] = "text/html";
	ngx.say("<html><head><script>");
	local hash = ngx.var.arg_name;
	if (hash == '') then
		ngx.say("document.cookie = 'ngx_envhash=; domain="..ngx.var.sname..'.'.ngx.var.tld.."; Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';");
		ngx.exit(ngx.OK);
	end

	local expires = 3600 * 24  -- 1 day
    ngx.header["Set-Cookie"] = "ngx_envhash="..hash.."; domain="..ngx.var.sname..'.'.ngx.var.tld.."; Path=/; Expires=" .. ngx.cookie_time(ngx.time() + expires)

	if (func.file_exists('/var/www/html/'..hash..'/public/index.php')) then 
		ngx.say("document.location = '/';");
	else 
		ngx.say('</script><h1>Cannot find that branch system</h1>');
	end
	ngx.say("</script></head></html>");
	ngx.exit(ngx.OK);
end

ngx.exit(ngx.OK);
