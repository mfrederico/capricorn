ngx.header['Content-Type'] = 'application/json';

require 'resty.core';

local cjson = require 'cjson';
local mysql = require 'resty.mysql';
local lrucache = require "resty.lrucache"

local db = mysql:new();

-- Cache my env file for 60 seconds .. 
local env	= ngx.shared.config;
local user	= '';

local config = env:get('config_'..ngx.var.host)

if not config then
	local cap = ngx.location.capture("/api/getenv/DB_CONNECTION,DB_READHOST,DB_PORT,DB_DATABASE,DB_USERNAME,DB_PASSWORD");
	env:set('config_'..ngx.var.host, cap.body, 60)
    config = env:get('config_'..ngx.var.host)
	ngx.log(ngx.DEBUG, 'RELOADING CONFIG');
else
	ngx.log(ngx.DEBUG, 'HIT CONFIG');
end

local conf = cjson.decode(config);

-- Connect to the database
local ok, err, errcode, sqlstate = db:connect({
	host = conf.DB_READHOST,
	port = conf.DB_PORT,
	database = conf.DB_DATABASE,
	user = conf.DB_USERNAME,
	password = conf.DB_PASSWORD})

-- Grab my apex user
if not ok then
	ngx.say(cjson.encode(err));
	ngx.exit(404);
else
	user = env:get('user_'..'1'..ngx.var.host);
	if not user then 
		ngx.log(ngx.DEBUG, 'RELOADING USER');
		local res, err, errcode, sqlstate = db:query("select first_name,last_name,email,password from users where id=1")
		if not err  then
			env:set('user_'..'1'..ngx.var.host,cjson.encode(res));
		else 
			ngx.say(cjson.encode(err));
		end
	else
		ngx.log(ngx.DEBUG, 'USER HIT');
	end

	ngx.say(env:get('user_'..'1'..ngx.var.host));
end

local ok, err = db:set_keepalive(10000, 50)
if not ok then
	ngx.log(ngx.ERR, "failed to set keepalive: ", err)
	ngx.exit(500)
end

ngx.exit(200);
