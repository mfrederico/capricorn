require 'resty.core';
local str			= require "resty.string"
local cjson			= require "cjson"
local lfs			= require "lfs"
local auth			= require 'cpad.authentication';
local myfunc		= require 'cpad.functions';


local FPATH="/home/ubuntu/bin:/home/ubuntu/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

local configjson,err = ngx.location.capture("/xpi/getenv/DB_READHOST,DB_PASSWORD,DB_USERNAME,DB_PORT,DB_DATABASE,APP_URL");

local conf = cjson.decode(configjson.body);

local STARTDATE = ngx.var.arg_start or ngx.today()..' 00:00:00' 
local ENDDATE	= ngx.var.arg_end or ngx.localtime();

local coreEnv = ngx.var.new_root..'../.env'
local envfile = coreEnv;

-- If we actually found an env file
if (ngx.var.envfile ~= 'NOTFOUND') then
    envfile = coreEnv..'.'..ngx.var.envfile;
end

function jsDate(date)
	local splitty = date:split('-');
	-- Javascript thought it would be cool to index months starting at 0
	local mo=splitty[2];
	mo = mo -1;
	return 'new Date('..splitty[1]..','..mo..','..splitty[3]..')';
end

function runCommand(title, tag, command, errorRegex)
	ngx.say('<div class="container">');
	ngx.say('<div class="row">');
	ngx.say('<h3 class="text-primary">'..title..'</h3><pre>');
	if (command == nil) then
		ngx.say('No command found! Aborting .. ');
		ngx.log(ngx.INFO,'* COMMAND NOT SENT')
		ngx.exit(ngx.OK);
	end
	ngx.say('<small>'..title..'</small>');
	local cv = spawnCommand(title, tag, command, errorRegex);
	ngx.say('</pre></div></div>');

	return cv
end

function spawnCommand(title, tag, command, errorRegex)
	ngx.flush();
    ngx.log(ngx.INFO,'* Running command: '.. title)
	local handle = io.popen(command.." 2>&1")
	local lc = 0 -- Line Count
	if (handle) then
		local line = assert(handle:read('*a'))
			lc = lc + 1;
			if (string.sub(line,1,8) == 'warning:') then 
				ngx.say('<span class="bg-warning">'..tag..': '..line..'</span>');
			elseif (string.sub(line,1,6) == 'fatal:') then 
				ngx.say('<span class="bg-danger">'..tag..': '..line..'</span>');
				ngx.exit(ngx.OK);
			else
				ngx.say(tag..': '..line);
			end
			ngx.flush();
		--end
		handle:close();
	else 
		ngx.say(tag..' '..command..' - *NOT FOUND*');
	end
	return lc;
end

local function printHeader() 
	ngx.header['Content-Type'] = 'text/html';
	ngx.say('<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></style><link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css"></style><script src="//code.jquery.com/jquery-1.12.4.js"></script><script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script><script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>');
end

if (ngx.var.arg_getLink) then
	local reportname = 'CUSTOM'
	-- so dangerous.. 
	local reportfile = io.open("/tmp/"..ngx.var.arg_getLink..'.tsv');
    if (reportfile ~= nil) then
		ngx.header['Content-Type'] = 'text/plain';
		ngx.header['Content-Disposition'] =  'attachment; filename="'..ngx.today()..'-'..reportname..'-'..conf.APP_URL..'.tsv"';
        ngx.say(reportfile:read("*all"));
        reportfile:close();
	else 
		ngx.say('Empty');
    end
end


auth.HTTP(nil, envfile);

if (ngx.var.arg_get) then
	local hash 		= str.to_hex(conf.DB_DATABASE..STARTDATE..ENDDATE);
	local reportpath = "/home/ubuntu/tools/reports/";

	-- Set up the jquery crap
	printHeader();

	if (myfunc.file_exists(reportpath..'/allorders.php')) then
		lfs.chdir(reportpath);

		local pullcurrent = '/usr/bin/php '..reportpath..'/allorders.php "'..ngx.unescape_uri(STARTDATE)..'::'..ngx.unescape_uri(ENDDATE)..'" '..conf.DB_USERNAME..' '..conf.DB_PASSWORD..' '..conf.DB_READHOST..' '..conf.DB_DATABASE..' | tee > /tmp/'..hash..'.tsv';
		local lc = runCommand('Pulling report', 'PULL', pullcurrent);
		if (myfunc.file_exists('/tmp/'..hash..'.tsv')) then
			ngx.say('<center><a type="button" class="btn btn-success" href="/xpi/reports/?getLink='..hash..'">Download your report</a>');
			ngx.say('<!-- '..pullcurrent .. ' -->');
		else 
			ngx.say('dont say dont say!');
		end
	end
end

ngx.exit(ngx.OK);

