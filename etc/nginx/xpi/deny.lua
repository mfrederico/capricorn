---------------------------
-- mfrederico nginx xpi  -- 
-- Firewall settings 1.0 --
---------------------------
local whitelist = {'127.0.0.1','50.213.140.225','23.90.4.81','173.231.12.83'}

local cjson     = require 'cjson';
local jsonResponse  = {}

------------------------
-- Functions          --
------------------------
local function addDeny(ip)
	local UFW       = "sudo /usr/sbin/ufw"
	local command     = UFW..' insert 1 deny from '..ip..'/24  to any comment "'..os.date()..'"';
    ngx.log(ngx.INFO,command);

	for i = 0,#whitelist do
		if (whitelist[i] == ngx.var.arg_ip) then
			ngx.log(ngx.WARN,'Skipping whitelisted IP');
			return "IP Whitelisted. Skipping";
		end
	end

    local handle = io.popen(command.." 2>&1")
    local entries   = '';
    if (handle) then
        local line = assert(handle:read('*a'))
        entries = line;
        io.close(handle);
		ngx.log(ngx.ERR,'DENY IP:'..ngx.var.arg_ip);
    end
    return entries;
end

jsonResponse.message        = addDeny(ngx.var.arg_ip)
jsonResponse.status         = 'success';
ngx.header['Content-type'] = 'application/json';

ngx.log(ngx.INFO,jsonResponse.message);
ngx.say(cjson.encode(jsonResponse));
