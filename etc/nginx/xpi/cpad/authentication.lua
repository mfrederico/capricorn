require 'resty.core';
local func = require 'cpad.functions'
local auth = {}

-- This is checking user / pass from mysql database
local function checkUserPass(un, pw, roletype, envfile)
	if (ngx.req.is_internal()) then
		ngx.log(ngx.INFO, 'INTERNAL REQUESTY'); 
		return true;
	end

	if (un and pw) then
		local mysql		= require 'resty.mysql';
		local str		= require "resty.string"
		local cjson		= require "cjson"

		-- local conf = func.loadEnvFile('/home/ubuntu/tools/settings.conf');
		local conf = func.loadEnvFile(envfile);

		if (conf == false) then 
			ngx.log(ngx.WARN, 'Configuration unable to load.');
			return false 
		end;

		-- You may have to define a resolver for nginx
		-- Connect to the database
		local db = mysql:new();
		local ok, err, errcode, sqlstate = db:connect({
			host		= conf.DB_READHOST,
			port		= conf.DB_PORT,
			database	= conf.DB_DATABASE,
			user		= conf.DB_USERNAME or un,
			password	= conf.DB_PASSWORD or pw
		})

		if (err) then
			ngx.log(ngx.ERR, cjson.encode(err));
			if (un ~= nil) then ngx.log(ngx.INFO, 'FAILED LOG IN: '..un); end
		else 
            if (ngx.var.http_x_forwarded_for) then
                ngx.log(ngx.INFO, 'LOGGED IN: '.. un .. ':IP:'..ngx.var.remote_addr..':FWDIP:'..ngx.var.http_x_forwarded_for)
            else
                ngx.log(ngx.INFO, 'LOGGED IN: '.. un .. ':IP:'..ngx.var.remote_addr)
            end

			return true;
		end

	end

	return false;
end


function auth.HTTP(roletype, envfile)
	if roletype == nil then roletype='Superadmin'; end;

	local cjson		= require "cjson"
	-- Make sure we are authenticated
	if (checkUserPass(ngx.var.arg_user, ngx.var.arg_pass, roletype, envfile) == true) then
		return true;
	elseif (checkUserPass(ngx.var.remote_user, ngx.var.remote_passwd, roletype, envfile) == true) then
		return true;
	else 
		ngx.header.www_authenticate = [[Basic realm="Restricted"]]
		ngx.exit(ngx.HTTP_UNAUTHORIZED);
		return false;
	end
end

return auth
