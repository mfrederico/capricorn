---------------------------------------------------------------
-- 'member to add /etc/nginx/xpi/?.lua to lua_package_path
---------------------------------------------------------------
-- module("functions", package.seeall)

local certloc		= "/etc/letsencrypt/live/"
local certbotloc    = "/home/ubuntu/tools/scripts/runcertbot.sh"
local defaultcert   = "/home/ubuntu/tools/etc/ssl/"
local func = {}

-- Convert from table to CSV string
function func.toCSV (tt)
  local s = ""
-- ChM 23.02.2014: changed pairs to ipairs 
-- assumption is that fromCSV and toCSV maintain data as ordered array
  for _,p in ipairs(tt) do  
    s = s .. "," .. func.escapeCSV(p)
  end
  return string.sub(s, 2)      -- remove first comma
end

function func.escapeCSV (s)
  if string.find(s, '[,"]') then
    s = '"' .. string.gsub(s, '"', '""') .. '"'
  end
  return s
end

function func.runcommand(command)
    local handle = io.popen(command.." 2>&1")
    local lc = 0 -- Line Count
    local data = "";
    if (handle) then
        data = data..assert(handle:read('*a'))
        handle:close();
    end
    return data;
end


function string:split( inSplitPattern, outResults )
  if not outResults then
    outResults = { }
  end
  local theStart = 1
  local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  while theSplitStart do
    table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
    theStart = theSplitEnd + 1
    theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  end
  table.insert( outResults, string.sub( self, theStart ) )
  return outResults
end

function func.getAWSmachineinfo()
    -- AWS specific instance info
    local instanceIP = '169.254.169.254';

    local sock = ngx.socket.tcp()
    sock:settimeout(1000)

    -- Get this servers aws settings identification
    local ok, err = sock:connect(instanceIP, 80)
    local bytes, err = sock:send("GET /latest/dynamic/instance-identity/document HTTP/1.1\r\n")
    local bytes, err = sock:send("Host: 169.254.169.254\r\n\r\n")

    local header = sock:receiveuntil("\r\n\r\n")
    local reader = sock:receiveuntil("\n}", { inclusive = true })

    local header    = header();
    local jsonstr   = reader();

    sock:close();

	local cjson = require 'cjson'
    return cjson.decode(jsonstr);
end


function func.splituri(uri)
        local suri = func.envsplit(uri,'getenv/');
        if (suri ~= null) then 
                local ms = suri[2]:split(','); 
                return ms;
        else
                return '*';
        end
end

function func.envsplit(inputstr, d)
        local splitty,err = ngx.re.match(inputstr, "^(.*?)"..d.."(.+?)$")
        if (not err) then 
                return splitty
        else
                return null;
        end;
end

function func.file_exists(filename)
    ngx.log(ngx.INFO,'* Checking File existence: '..filename)
    local file = io.open(filename)
    if not file then
        ngx.log(ngx.DEBUG,' File doesnt exist!');
        return false;
    end
    file:close()
    ngx.log(ngx.DEBUG,' File EXISTS!');
    return true;
end

function func.basename(str)
	local name = string.gsub(str, "(.*/)(.*)", "%2")
	return name
end

function func.writeEnvFile(envfile, values)
	local cjson = require 'cjson'
	local envValues = {}
	--local file = io.open('/tmp/'..basename(envfile), "w+");
	local file = io.open(envfile, "w+");
	file:write('# UPDATED: '..os.date('%c').."\n");
	file:write('# BY     : '..ngx.var.remote_user.."\n");
	file:write('# IP     : '..ngx.var.remote_addr.."\n");
	if (ngx.var.http_x_forwarded_for ~= nil) then
		file:write('# FWDIP  : '..ngx.var.http_x_forwarded_for.."\n");
	end
	local itemcount = 0;
	if (file ~= nil) then
		local tkeys = {}
		-- populate the table that holds the keys
		for k in pairs(values) do table.insert(tkeys, k) end
		-- sort the keys
		table.sort(tkeys)
		-- use the keys to retrieve the values in the sorted order
		for _, k in ipairs(tkeys) do 
			file:write(k..'='..values[k].."\n");
			itemcount = itemcount +1;
		end
		file:close();
		return itemcount
	end
	file:close();
	return false;
end

function func.loadSharedSettings(envfile, sharedvar)
	local tkeys = func.loadEnvFile(envfile);
	local cjson = require 'cjson'
	ngx.log(ngx.ERR,'ENV: '..envfile..' '..cjson.encode(tkeys));

	for k, v in pairs(tkeys) do 
		ngx.log(ngx.ERR,'K: '..k..' V:' ..v);
		sharedvar:set(k,v);
	end
	return true;
end

function func.loadEnvFile(envfile)
	local envValues = {}
	local file = io.open(envfile, "r");

	if (file ~= nil) then
		ngx.log(ngx.INFO,envfile..' FOUND')
	else
		ngx.log(ngx.INFO,envfile..' NOT found');
	end

	if (file ~= nil) then
		for line in file:lines() do
			local myval = func.envsplit(line,'=');
			if (myval ~= null) then
				envValues[myval[1]] = myval[2];
			end
		end
		return envValues
	else
		return false;
	end
	
end

    -----------------------------------
    -- Parse our magical hostname
    -----------------------------------
    function func.parseHostname(mydomain, include_subdomain)
        local parts = {}
        local returnparsed = '';


        local parts = mydomain:split('%.');

		-- This will default if no hostname to the current system hostname
		if (#parts < 2) then
			ngx.log(ngx.INFO,'Server Hostname: '..ngx.var.hostname);
			parts = ngx.var.hostname:split('%.');

			-- Accomodate single name hosts e.g.: mydev-laptop
			if (#parts < 2) then 
				ngx.log(ngx.DEBUG,'Develop Server: '..ngx.var.hostname..'.dev');
				parts[2] = 'dev';
				parts[1] = ngx.var.hostname
			end

		end

        local tld       = parts[(#parts)];
        local domain    = parts[(#parts)-1];
		local publicid  = {}

        -- Parse the parts of the domain
        if (#parts > 2) then
            publicid = parts[1];
        else
            publicid = 'www'; -- the DOT will automatically be put in
        end

        ngx.log(ngx.INFO, 'subdomain :'..publicid);
        ngx.log(ngx.INFO, 'domain    :'..domain);
        ngx.log(ngx.INFO, 'tld       :'..tld);
		returnparsed = publicid..'.'..domain..'.'..tld;
        ngx.log(ngx.INFO,'PARSED:  '..returnparsed)
        return returnparsed;
    end

    function func.my_load_certificate_chain(ssl)
        if (ssl.server_name()) then
			local cjson = require 'cjson'
            local servername = ssl.server_name()

			-- Check the ssl certificate cache
			local certcache,sslflags,sslexpire =  ngx.shared.ssldata:get(servername..'_chain')
			if (certcache ~= nil) then 
				ngx.log(ngx.INFO,'************ USING CACHED CERT! ****************')
				return certcache 
			end
			ngx.log(ngx.INFO,'************ LOADING CERT! ****************')

            local parts = servername:split('%.');
            local filename = '';

            local hascert   = false;

            local filecontents = io.lines(defaultcert.."default/fullchain.pem", "*a")();

            for i = 1,#parts,1 do
				filename = '';

				for p = i,#parts,1 do
					filename = filename..(p ~= i and '.' or '')..parts[p]
				end
				ngx.log(ngx.INFO,'Looking for: '.. certloc..filename..'/fullchain.pem');
				if (func.file_exists(certloc..filename..'/fullchain.pem')) then
					ngx.log(ngx.INFO, "Loading Chain: " .. ssl.server_name() .. ' / '..filename.. ' / '..servername)
					filecontents = io.lines(certloc..filename.."/fullchain.pem", "*a")();

					-- Set the certificate cache
					ngx.shared.ssldata:set(servername..'_chain',filecontents,60)
					return filecontents;
				end
            end

			--[[ local naked_ip = ngx.re.match(ngx.var.host,"^([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3})$");
			if not naked_ip then
				-- Issue the cert .. 
				ngx.log(ngx.ERR, "**************** Issuing new certificate: "..servername);
				handle = io.popen(certbotloc..' '..servername);
				handle:close()
				filecontents = io.lines(certloc..servername.."/fullchain.pem", "*a")();
			end
			]]
			return filecontents;
        else
            ngx.log(ngx.ERR,'Cannot parse hostname!');
            ngx.exit(ngx.ERROR);
        end
    end

    function func.my_load_private_key(ssl)
		local servername = ssl.server_name()

        local parts = servername:split('%.');
        local filename = '';

		-- Default
        local filecontents = io.lines(certloc.."default/privkey.pem", "*a")();

		-- Check the ssl certificate cache
		local certcache,sslflags,sslexpire =  ngx.shared.ssldata:get(servername..'_privkey')
		if (certcache ~= nil) then 
			ngx.log(ngx.INFO,'************ USING CACHED PRIVKEY! ****************')
			return certcache 
		end

		ngx.log(ngx.INFO,'************ LOADING PRIVKEY! ****************')

		for i = 1,#parts,1 do
			filename = '';
            for p = i,#parts,1 do
                filename = filename..(p ~= i and '.' or '')..parts[p]
            end
			ngx.log(ngx.INFO,'Looking for: '.. certloc..filename..'/privkey.pem');
			if (func.file_exists(certloc..filename..'/privkey.pem')) then
				ngx.log(ngx.INFO, "Loading Chain: " .. ssl.server_name() .. ' / '..filename.. ' / '..servername)
				filecontents = io.lines(certloc..filename.."/privkey.pem", "*a")();
				ngx.shared.ssldata:set(servername..'_privkey',filecontents,60)
				return filecontents;
			end
		end
        return filecontents;
    end

return func
