-- Set this by HOST?
ngx.var.jwt_secret = "cjIF2rhpSRXALLwEsF2FkM6rYpk2o4ox";

local jwt = require "resty.jwt" 
local headers = ngx.req.get_headers() 
local auth_header = nil; 

-- ngx.var.no_cache	= 1;

local h = ngx.req.get_headers() 
for k, v in pairs(h) do 
	if k == 'authorization' then  
		auth_header = v; 
		break;
	end 
end 

if auth_header then 
	local _, _, jwt_token = string.find(auth_header, "Bearer%s+(.+)") 

	local jwt_obj = jwt:verify(ngx.var.jwt_secret, jwt_token); 
	local cjson   = require "cjson" 

	if not jwt_obj["verified"] then 
		ngx.log(ngx.INFO,'INVALID JWT');
		return nil;
	else  
		ngx.var.no_cache	= 0;
		ngx.log(ngx.DEBUG,'YES! JWT VERIFIED:'.. cjson.encode(jwt_obj)); 
		return jwt_obj.payload.orgId;
	end 
end 
