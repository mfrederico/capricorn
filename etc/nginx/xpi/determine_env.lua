local folders = {}
-- Add folders in numeric order to scan correctly
folders[1] =  { ['scan'] = '/var/www/html/default/%sname%/public/index.php',  ['root'] = '/var/www/html/default/%sname%/public/'}
folders[2] =  { ['scan'] = '/var/www/html/default/%sname%/public/index.html', ['root'] = '/var/www/html/default/%sname%/public/'}
folders[3] =  { ['scan'] = '/var/www/html/default/%sname%/index.php', 		  ['root'] = '/var/www/html/default/%sname%/' }
folders[4] =  { ['scan'] = '/var/www/html/default/%sname%/index.html', 		  ['root'] = '/var/www/html/default/%sname%/' }
folders[5] =  { ['scan'] = '/var/www/html/wordpress/%sname%/index.php',		  ['root'] = '/var/www/html/wordpress/%sname%/'}

local ok, upstream = pcall(require, "ngx.upstream")
if not ok then
    error("ngx_upstream_lua module required")
end

local myfunc		= require('cpad/functions');
local cjson     	= require "cjson"
local sitedata  = {}

local hostparts		= ngx.var.host:split('%.');
local sname  		= '';
local dositecache	= 0;

------------------------------
-- Header based environment --
-- Check for JWT here       --
------------------------------
local h = ngx.req.get_headers()
for hheader, hvalue in pairs(h) do
	if (hheader == 'x-cp-client-domain') 
	then
		ngx.log(ngx.DEBUG,"------- HEADERS "..hvalue..' --------');
		hostparts = hvalue:split('%.');
		break;
	end
end

ngx.var.tld			= hostparts[#hostparts];
table.remove(hostparts, #hostparts); -- drop TLD

-- ngx.var.publicid	= 
if (hostparts[1] ~= nil) then ngx.var.publicid=hostparts[1] else ngx.var.publicid='www' end

ngx.var.sname		= table.concat(hostparts,'.');

ngx.log(ngx.DEBUG,'*** PARTICLES: '..ngx.var.publicid..' '..ngx.var.sname..' '..ngx.var.tld..' ');

----------------------------------------
-- DO we have this information cached --
----------------------------------------
if (dositecache ~= nil) then
	local sitecache,siteflags,siteexpire =  ngx.shared.sitedata:get(ngx.var.sname)
	if (sitecache ~= nil) then
		ngx.log(ngx.INFO,'*** LOOKUP CACHE HIT: '..ngx.var.sname);
		ngx.log(ngx.DEBUG,'*** '..cjson.encode(sitecache))
		local scdata = cjson.decode(sitecache);
		ngx.var.publicid   = scdata.publicid
		ngx.var.sname	   = scdata.sname
		ngx.var.tld	 	   = scdata.tld
		ngx.var.envfile    = scdata.envfile
		ngx.var.new_root   = scdata.new_root
		ngx.var.fpmbackend = scdata.fpmbackend
		return scdata.envfile
	else 
		ngx.log(ngx.INFO,'*** LOOKUP CACHE MISS ***');
	end
end

-----------------------------------------------------
-- IF we're running a reverse proxy at this domain --
-----------------------------------------------------
local proxydata		= require('determine_proxy');
if (proxydata ~= false) then
	ngx.log(ngx.INFO,'*** PROXY? ***'..cjson.encode(proxydata));
	ngx.var.proxyhost	 = proxydata.HOST;
	ngx.var.proxyport	 = proxydata.PORT;
	ngx.var.proxyschema  = proxydata.SCHEMA;
	ngx.var.proxybaseuri = proxydata.BASEURI;
else
	------------------------------------------------------------
	-- Scan for folders that have our closest literal domain ---
	------------------------------------------------------------
	local directmatch = false; -- If we find a folder that is a direct match
	for foldermatch, folder in ipairs(folders) do
		ngx.log(ngx.INFO,'***  ORDER OF FOLDERS: '..folder['scan'])
	end

	for idx, folderscan in pairs(folders) do
		local foldermatch	= folderscan['root']
		local folder		= folderscan['scan']
		local hostparts		= ngx.var.sname:split('%.');
		local folderfound = nil;
		local findfolder        = '';
		local userfolder        = '';
		if directmatch then break end;

		ngx.log(ngx.INFO,'***  CHECKING FOLDER: '..folder)

		while (#hostparts > 0) do
			-- Replace the short.servername in here
			local servername = table.concat(hostparts,'.');
			if (#hostparts == 0) then 
				break 
			end

			findfolder	= foldermatch:gsub('%%sname%%', servername);
			userfolder	= foldermatch:gsub('%%sname%%', servername);

			ngx.log(ngx.INFO,'***  REDUCE: '..table.concat(hostparts,'.')..' FIND FOLDER: '..findfolder .. ' USERFOLDER: '..userfolder);

			table.remove(hostparts, 1); 

			-- Did we find a folder
			if (myfunc.file_exists(findfolder)) then 

				folderfound			= userfolder
				ngx.log(ngx.INFO,'*** ROOT FOLDER: '..folderfound);

				-- Quickly sweep for any possible .env files
				local envparts		= ngx.var.sname:split('%.');
				ngx.var.envfile = 'NOTFOUND';
				ngx.var.envfile = 'env';

				while (#envparts > 0) do
					local envname=table.concat(envparts,'.');
					ngx.log(ngx.INFO,'*** ENVCHECK : '..folderfound..'../.env.'..envname);
					if (myfunc.file_exists(folderfound..'../.env.'..envname)) then
						ngx.var.envfile = envname;
					end
					table.remove(envparts,1);
				end

				directmatch			= true;
				ngx.var.new_root	= folderfound; 

				break;
			end

		end

		if (folderfound ~= nil) then 
			break 
		end
	end
	--------------------------------------------------------
	-- Default my php to prodorktion 
	--------------------------------------------------------
	local fpmservers = upstream.get_upstreams();

	ngx.var.fpmbackend = 'production';

	for _, fpmserver in ipairs(fpmservers) do
		if (fpmserver == ngx.var.sname) then 
			ngx.log(ngx.DEBUG, '* Checking upstream:'..fpmserver);
			ngx.var.fpmbackend = fpmserver;
			break;
		end
		if (ngx.var.tld == 'dev') then
			ngx.log(ngx.DEBUG, '* Default upstream');
			ngx.var.fpmbackend = 'default';
			break;
		end
	end
	ngx.log(ngx.DEBUG, '*********** CACHING THE FOLLOWING FOR 60 *************')
	ngx.log(ngx.DEBUG, 'DOCROOT   : '..cjson.encode(ngx.var.new_root));
	ngx.log(ngx.DEBUG, 'UPSTREAMS : '..cjson.encode(fpmservers));
	ngx.log(ngx.DEBUG, 'FPM SERVER: '..ngx.var.fpmbackend);
	ngx.log(ngx.DEBUG, 'ENVFILE   : '..ngx.var.envfile);

	-- Set up 60 second caching for this info
	sitedata.publicid	 = ngx.var.publicid;
	sitedata.sname		 = ngx.var.sname;
	sitedata.tld		 = ngx.var.tld;
	sitedata.envfile	 = ngx.var.envfile; 
	sitedata.new_root	 = ngx.var.new_root; 
	sitedata.fpmbackend	 = ngx.var.fpmbackend; 

	local a,b,c = ngx.shared.sitedata:set(ngx.var.sname,cjson.encode(sitedata),10)

	ngx.log(ngx.DEBUG, '*******************************************************: A '..cjson.encode(a)..' B '..cjson.encode(b))

	return ngx.var.envfile;
end
